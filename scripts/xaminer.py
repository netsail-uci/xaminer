from cross_layer_map.ip_geolocation_process import get_best_geolocation_for_all_ips
from cross_layer_map.ip_to_asn_process import generate_ip_to_asn_map
from cross_layer_map.nautilus_process import load_nautilus_mapping, generate_cable_segments_to_all_info_map, generate_cable_segments_to_all_info_map_all_cable_top_segments, generate_cable_segments_to_all_info_map_random
from cross_layer_map.submarine_process import LandingPoints
from map_embedding.map_embedding import generate_cable_segment_to_country_as_maps
from resilience_analysis.resilience_analysis import load_ases_data, get_full_stats_for_each_layer, get_full_stats_aggregated, process_event_ranges, process_event_global, process_event_baseline_ranges, process_multiple_events_combined, event_independent_analysis, generate_country_correlation_cluster, singular_event_impact_analysis, process_event_inaccuracies_accounted
from resilience_analysis.plot_utils import *
from copy import deepcopy
import multiprocessing
from tqdm import tqdm

def sensitivity_analysis_helper (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, accuracy):

	cable_segment_to_all_info_map_random = generate_cable_segments_to_all_info_map_random (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, accuracy)
	_, _, cable_segment_against_country_random, _, _ = generate_cable_segment_to_country_as_maps(cable_segment_to_all_info_map_random, ases)

	return cable_segment_against_country_random


if __name__ == '__main__':

	event = {'category_condition': 'oc', 'ip_version': 4}

	# Loading the cross-layer map, IP geolocation, and IP to ASN mapping results
	conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
	ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
	ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

	# Aggregating the prior results to select best mapping results (ie., select only the top cable segment as mapping for a given IP link)
	print (f'Generating the cable segment to all information map')
	cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

	# Getting the list of all ASes (as we will need this information for later)
	ases = load_ases_data(cable_segment_to_all_info_map)

	# cable_segment_against_country (CS-NC) and cable_segment_against_as (CS-AS)
	# countries and ases variable represent all the countries and ASes being considered 
	# metadata has information on mappings between the original IP and AS data and link information to corresponding index representation used for memory constraints
	print (f'Generating the concise representations')
	countries, ases, cable_segment_against_country, cable_segment_against_as, metadata = generate_cable_segment_to_country_as_maps(cable_segment_to_all_info_map, ases)

	with open('results/concise_representations', 'wb') as fp:
		pickle.dump((countries, ases, cable_segment_against_country, cable_segment_against_as, metadata), fp)

	# This includes stats (counts) of Cable segments, Cables, IP links, IPs, AS links, and ASes respectively
	# Set return_mode to 1 if instead of the counts, the actual index representations are needed (metadata from above can be used to map the index back to the corresponding IP (link) / AS (link) entity)
	# Set return_mode to anything else if both counts and data are needed (the return value will be a tuple (counts, data))
	print (f'Generating stats for each layer')
	total_stats = get_full_stats_for_each_layer (cable_segment_against_country, metadata, return_mode=0)

	# Aggregating the results at a country and AS level respectively
	print (f'Generating stats for each layer per country/AS')
	country_stats_aggregated, as_stats_aggregated, country_stats_aggregated_data, as_stats_aggregated_data = get_full_stats_aggregated ((cable_segment_against_country, cable_segment_against_as), countries, ases, country_or_as=3, return_mode=1)

	with open('results/stats_aggregated', 'wb') as fp:
		pickle.dump((total_stats, country_stats_aggregated, as_stats_aggregated, country_stats_aggregated_data, as_stats_aggregated_data), fp)
	
	# NOTE: FOR ALMOST ALL OUR FUNCTIONS, INTERNALLY WE WILL CALL THE "process_single_event" FUNCTION (IN scripts/resilience_analysis/resilience_analysis.py FILE). LOOK AT THE COMMENTS UNDER THAT FUNCTION TO UNDERSTAND HOW TO USE IT FOR OTHER PURPOSES
	print (f'Initiating processing of user-given arguments')

	# The following flag will be set when running experiments corresponding to sections 5.1 and 5.3.1 (including Appendix B.1 and B.3)
	disaster_combinations = False

	if disaster_combinations:

		# Examining various probabilities of failures and sampling distribution (given by possible_retains and possible_choices respectively)
		# For sampling distributions that involve randomness, results are averaged over 10 runs
		possible_retains = [0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
		possible_choices = ['random'] * 10 + ['top'] + ['prob_top'] * 10

		event_tags, all_range_stats = [], []
		
		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['jp']},
			'tag': 'Japan-earthquake'
		}

		event.update(event_updates)

		jp_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(jp_stats)

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['cu', 'jm', 'ht', 'do', 'bs']},
			'tag': 'Carribbean-Hurricane'
		}
		
		event.update(event_updates)

		crb_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(crb_stats)
		
		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'regions': ['Washington', 'Oregon', 'British Columbia']},
			'tag': 'PNW-earthquake'
		}

		event.update(event_updates)

		pnw_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(pnw_stats)

		plot_maximal_impact_for_given_events (event_tags, all_range_stats, 'plots/mpi_regional_plot_1.pdf')

		plot_probablistic_impact_for_given_events (['Japan Earthquake', 'Carribbean Hurricane'], [jp_stats, crb_stats], {'Japan Earthquake': ['random', 'top', 'prob_top'], 'Carribbean Hurricane': ['top']}, 'plots/probablisitic_impact_1.pdf', 2)
		
		event_tags, all_range_stats = [], []

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['id']},
			'tag': 'Indonesia-earthquake'
		}
		
		event.update(event_updates)

		id_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(id_stats)

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['tw', 'ph']},
			'tag': 'TW-PH-earthquake'
		}

		event.update(event_updates)

		# In the process_event_ranges, we internally call the "process_single_event" function which first identifies the impacted cable segments and then return the releavant results as needed by each experiment
		tw_ph_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(tw_ph_stats)

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['gb', 'ie']},
			'tag': 'British-Isles-Hurricane'
		}
		
		event.update(event_updates)

		bis_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(bis_stats)

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['bz', 'hn', 'ni', 'pa', 'cr']},
			'tag': 'Central-America-Hurricane'
		}
		
		event.update(event_updates)

		car_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(car_stats)

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'regions': ['Florida', 'Louisiana', 'Texas']},
			'tag': 'Florida-Hurricane'
		}
		
		event.update(event_updates)

		fl_stats = process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_country, total_stats)
		event_tags.append(' '.join([item for item in event['tag'].split('-')]))
		all_range_stats.append(fl_stats)

		plot_maximal_impact_for_given_events (event_tags, all_range_stats, 'plots/mpi_regional_plot_2.pdf', (0.5, 0.95), 3, True)

		plot_probablistic_impact_for_given_events (['Carribbean Hurricane', 'PNW Earthquake', 'British Isles Hurricane'], [crb_stats, pnw_stats, bis_stats], {'PNW Earthquake': ['random', 'top', 'prob_top'], 'BIS Hurricane': ['random', 'top', 'prob_top'], 'Carribbean Hurricane': ['random', 'prob_top']}, 'plots/probablisitic_impact_2.pdf', 4)
		plot_probablistic_impact_for_given_events (['TW PH Earthquake', 'Central America Hurricane'], [tw_ph_stats, cam_stats], {'TW PH Earthquake': ['random', 'top', 'prob_top'], 'Central America Hurricane': ['random', 'top', 'prob_top']}, 'plots/probablisitic_impact_3.pdf', 3)
		plot_probablistic_impact_for_given_events (['Indonesia Earthquake', 'Florida Hurricane'], [id_stats, fl_stats], {'Indonesia Earthquake': ['random', 'top', 'prob_top'], 'Florida Hurricane': ['random', 'top', 'prob_top']}, 'plots/probablisitic_impact_4.pdf', 3)

	# The following flag will be set when running experiments corresponding to sections 5.2.1 and 5.2.2 (including Appendix B.2)
	disaster_global = False

	if disaster_global:

		# A few default updates for this experiment
		event_updates = {
			'type': 'disaster', 
			'scope': None,
			'additional_threshold': {'special': 0}
		}

		event.update(event_updates)

		event_tags, all_range_stats = [], []

		# Next, we do each disaster (earthquake, hurricane, sea rise and solar storm)

		event_updates = {
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'tag': 'earthquake'
		}

		event.update(event_updates)

		# Since we are looking at the results at a country-level, we use cable_segment_against_country and country_segments_aggregated and set country_or_as to 1 (to indicate we are processing at a country level)
		earthquake_p1_stats, earthquake_p2_stats, earthquake_p3_stats = process_event_global (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_stats_aggregated_data, metadata['intra-inter'], country_or_as=1)
		event_tags.append(event['tag'].capitalize())
		all_range_stats.append(earthquake_p1_stats)

		plot_country_level_impact (earthquake_p2_stats, 'plots/country_earthquake_impact.pdf', disaster='Earthquake', special=None, threshold='6 PGA')
		plot_country_level_intra_inter_distribution (earthquake_p3_stats, 'plots/country_earthquake', disaster='Earthquake', column='Intra', special=None)
		plot_country_level_intra_inter_distribution (earthquake_p3_stats, 'plots/country_earthquake', disaster='Earthquake', column='Inter', special=None)
		
		event_updates = {
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'tag': 'hurricane'
		}

		event.update(event_updates)

		hurricane_p1_stats, hurricane_p2_stats, hurricane_p3_stats = process_event_global (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_stats_aggregated_data, metadata['intra-inter'], country_or_as=1)
		event_tags.append(event['tag'].capitalize())
		all_range_stats.append(hurricane_p1_stats)

		plot_country_level_impact (hurricane_p2_stats, 'plots/country_hurricanee_impact.pdf', disaster='Hurricane', special=None, threshold='64 knots')
		plot_country_level_intra_inter_distribution (hurricane_p3_stats, 'plots/country_hurricane', disaster='Hurricane', column='Intra', special=None)
		plot_country_level_intra_inter_distribution (hurricane_p3_stats, 'plots/country_hurricane', disaster='Hurricane', column='Inter', special=None)

		event_updates = {
			'name': 'searise',
			'baseline': 1,
			'search_radius': 10,
			'tag': 'searise'
		}

		event.update(event_updates)

		searise_p1_stats, searise_p2_stats, searise_p3_stats = process_event_global (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_stats_aggregated_data, metadata['intra-inter'], country_or_as=1)
		event_tags.append(event['tag'].capitalize())
		all_range_stats.append(searise_p1_stats)

		plot_country_level_impact (searise_p2_stats, 'plots/country_searise_impact.pdf', disaster='Searise', special='Chad')
		plot_country_level_intra_inter_distribution (searise_p3_stats, 'plots/country_searise', disaster='Searise', column='Intra', special='Chad')
		plot_country_level_intra_inter_distribution (searise_p3_stats, 'plots/country_searise', disaster='Searise', column='Inter', special='Chad')

		event_updates = {
			'name': 'solarstorm',
			'baseline': 50,
			'search_radius': 1,
			'tag': 'solarstorm'
		}

		event.update(event_updates)

		solarstorm_p1_stats, solarstorm_p2_stats, solarstorm_p3_stats = process_event_global (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_stats_aggregated_data, metadata['intra-inter'], country_or_as=1)
		event_tags.append(event['tag'].capitalize())
		all_range_stats.append(solarstorm_p1_stats)

		plot_country_level_impact (solarstorm_p2_stats, 'plots/country_solarstorm_impact.pdf', disaster='Solarstorm', special=None, threshold='50 latitude')
		plot_country_level_intra_inter_distribution (solarstorm_p3_stats, 'plots/country_solarstorm', disaster='Solarstorm', column='Intra', special=None)
		plot_country_level_intra_inter_distribution (solarstorm_p3_stats, 'plots/country_solarstorm', disaster='Solarstorm', column='Inter', special=None)

		plot_maximal_impact_for_disasters_global (event_tags, all_range_stats, 'plots/mpi_global_plot.pdf', ylim=(0, 80), bbox_position=(0.5, 0.95), legend_cols=3)
	
	# The following flag will be set when running experiments corresponding to sections 5.2.3
	disaster_ranges = False

	if disaster_ranges:

		# A few default updates for this experiment
		event_updates = {
			'type': 'disaster', 
			'scope': None,
			'additional_threshold': {'special': 0},
		}

		event.update(event_updates)

		possible_baselines = [0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2]

		event_updates = {
			'name': 'searise',
			'search_radius': 10,
			'tag': 'searise'
		}

		event.update(event_updates)

		searise_stats = process_event_baseline_ranges (possible_baselines, event, cable_segment_against_country, total_stats)
		plot_disaster_baseline_impacts (searise_stats, 'plots/searise_baseline_stats.pdf', disaster='Sea rise (in meters)')

		possible_baselines = [70, 65, 60, 55, 50, 45, 40]

		event_updates = {
			'name': 'solarstorm',
			'search_radius': 1,
			'tag': 'solarstorm'
		}

		event.update(event_updates)

		solarstorm_stats = process_event_baseline_ranges (possible_baselines, event, cable_segment_against_country, total_stats)
		plot_disaster_baseline_impacts (solarstorm_stats, 'plots/solarstorm_baseline_stats.pdf', disaster='Solar storm threshold (Degree of latitude)')
	
	# The following flag will be set when running experiments corresponding to sections 5.3.2
	multiple_disasters = False

	if multiple_disasters:

		# A few default updates for this experiment
		event_updates = {
			'type': 'disaster', 
			'scope': None
		}

		event.update(event_updates)

		# Making the list of events that we want to consider
		events = []

		event_updates = {
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
		}

		event.update(event_updates)
		events.append(deepcopy(event))

		event_updates = {
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
		}

		event.update(event_updates)
		events.append(deepcopy(event))

		event_updates = {
			'name': 'searise',
			'baseline': 0.3,
			'search_radius': 10,
			'additional_threshold': {'special': 0}
		}

		event.update(event_updates)
		events.append(deepcopy(event))

		event_updates = {
			'name': 'solarstorm',
			'baseline': 50,
			'search_radius': 1,
			'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
		}

		event.update(event_updates)
		events.append(deepcopy(event))

		multiple_p1_stats, multiple_p2_stats, multiple_p3_stats = process_multiple_events_combined (events, cable_segment_against_country, total_stats, country_stats_aggregated, country_stats_aggregated_data, metadata['intra-inter'], country_or_as=1)

		plot_stats_multiple_disasters_merged(multiple_p1_stats, 'plots/multiple_disasters_merged_impact.pdf')
		plot_country_level_impact (multiple_p2_stats, 'plots/country_multiple_disasters_merged_impact.pdf', disaster='All', special=None, threshold='')
		plot_country_level_intra_inter_distribution (multiple_p3_stats, 'plots/country_multiple_disasters_merged', disaster='All', column='Intra', special=None)
		plot_country_level_intra_inter_distribution (multiple_p3_stats, 'plots/country_multiple_disasters_merged', disaster='All', column='Inter', special=None)


	# The following flag will be set when running experiments corresponding to sections 5.4
	independent_analysis = False

	if independent_analysis:

		depedence_count_country, depedence_count_as, intra_as_count_per_country, intra_as_count_per_as, country_against_country = event_independent_analysis (cable_segment_against_country, country_stats_aggregated_data, as_stats_aggregated_data, metadata['intra-inter-ip'], infra_level='country')
		
		plot_dependence_count_by_country (depedence_count_country, 'plots/country_dependence_globe.pdf')
		plot_dependence_count_by_country (depedence_count_country, 'plots/country_dependence_europe.pdf', unwanted_continents=['Africa', 'Asia', 'North America', 'South America', 'Oceania'], unwanted_countries=['Russia', 'Fr. S. Antarctic Lands'], figsize=(10, 5), xlim=[-30, 40], ylim=[30,80], cax_position=[0.8, 0.15, 0.1, 0.7], special="Germany")

		plot_ecdf_plots (depedence_count_country, 'plots/country_ecdf_plot.pdf', xlim=150)
		plot_ecdf_plots (depedence_count_as, 'plots/as_ecdf_plot.pdf', xlim=60)

		plot_country_level_percent_intra_distribution (intra_as_count_per_country, 'plots/country_intra_percentage_dist.pdf')

		country_against_country_specific = {country: [item[3] for item in value] for country, value in country_against_country.items()}
		cluster_df, cluster_id_to_country = generate_country_correlation_cluster (country_against_country_specific, countries)
		generate_correlation_plot (cluster_df, 'plots/country_correlation_plot.pdf', len(cluster_id_to_country))

	# The following flag (singular_event) will be set when running experiments corresponding to sections 5.5
	# Note that this section had 2 methods (ie., Top and Weighted). If Top is needed, enable event_top flag, else enable event_weighted flag. Set both to True for both (top and weighted)
	# Note that atleast one of the 2 (event_top or event_weighted) should be set to True while running this experiment
	singular_event, event_top, event_weighted = False, False, False

	if singular_event:

		if event_top:

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5938, 3259), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259)]},
				'tag': 'SeaMeWe-5-cut-failure-segments-top'
			}

			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_or_as=1)

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4177), ('Asia Africa Europe-1 (AAE-1)', 3210, 4361), ('Asia Africa Europe-1 (AAE-1)', 3160, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3210), ('Asia Africa Europe-1 (AAE-1)', 6007, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3160), ('Asia Africa Europe-1 (AAE-1)', 5956, 3160), ('Asia Africa Europe-1 (AAE-1)', 3210, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9118), ('Asia Africa Europe-1 (AAE-1)', 3210, 5956), ('Asia Africa Europe-1 (AAE-1)', 3160, 4361), ('Asia Africa Europe-1 (AAE-1)', 9486, 6007), ('Asia Africa Europe-1 (AAE-1)', 3160, 9118), ('Asia Africa Europe-1 (AAE-1)', 9486, 3160), ('Asia Africa Europe-1 (AAE-1)', 3160, 14639), ('Asia Africa Europe-1 (AAE-1)', 3210, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 6013), ('Asia Africa Europe-1 (AAE-1)', 3210, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 3259), ('Asia Africa Europe-1 (AAE-1)', 14639, 3210), ('Asia Africa Europe-1 (AAE-1)', 3210, 9485), ('Asia Africa Europe-1 (AAE-1)', 4177, 3160), ('Asia Africa Europe-1 (AAE-1)', 6013, 3160), ('Asia Africa Europe-1 (AAE-1)', 9486, 3259), ('Asia Africa Europe-1 (AAE-1)', 3212, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4265), ('Asia Africa Europe-1 (AAE-1)', 9486, 5962), ('Asia Africa Europe-1 (AAE-1)', 3160, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 9485)]},
				'tag': 'Egypt-cut-failure-segments-top'
			}
			
			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_or_as=1)

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'landing_points': ['Al Hudaydah, Yemen']},
				'tag': 'Al-Hudaydah-failure-top'
			}
			
			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country, total_stats, country_stats_aggregated, country_or_as=1)

		if event_weighted:
			
			# For weighted mode, we will need a new cable_segment_to_all_info map as the ones used above considers only the best cable segment
			# Since there is a new cable_segment_to_all_info_map, the rest of the concise representations and aggregations need to be redone 
			
			cable_segment_to_all_info_map_weighted = generate_cable_segments_to_all_info_map_all_cable_top_segments (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)
			countries_weighted, ases_weighted, cable_segment_against_country_weighted, cable_segment_against_as_weighted, metadata_weighted = generate_cable_segment_to_country_as_maps(cable_segment_to_all_info_map_weighted, ases)
			total_stats_weighted = get_full_stats_for_each_layer (cable_segment_against_country_weighted, metadata_weighted, return_mode=0)
			country_stats_aggregated_weighted, as_stats_aggregated_weighted = get_full_stats_aggregated ((cable_segment_against_country_weighted, cable_segment_against_as_weighted), countries, ases, country_or_as=3)

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5938, 3259), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259)]},
				'tag': 'SeaMeWe-5-cut-failure-segments-weighted'
			}

			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country_weighted, total_stats_weighted, country_stats_aggregated_weighted, country_or_as=1, weighted=True)

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4177), ('Asia Africa Europe-1 (AAE-1)', 3210, 4361), ('Asia Africa Europe-1 (AAE-1)', 3160, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3210), ('Asia Africa Europe-1 (AAE-1)', 6007, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3160), ('Asia Africa Europe-1 (AAE-1)', 5956, 3160), ('Asia Africa Europe-1 (AAE-1)', 3210, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9118), ('Asia Africa Europe-1 (AAE-1)', 3210, 5956), ('Asia Africa Europe-1 (AAE-1)', 3160, 4361), ('Asia Africa Europe-1 (AAE-1)', 9486, 6007), ('Asia Africa Europe-1 (AAE-1)', 3160, 9118), ('Asia Africa Europe-1 (AAE-1)', 9486, 3160), ('Asia Africa Europe-1 (AAE-1)', 3160, 14639), ('Asia Africa Europe-1 (AAE-1)', 3210, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 6013), ('Asia Africa Europe-1 (AAE-1)', 3210, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 3259), ('Asia Africa Europe-1 (AAE-1)', 14639, 3210), ('Asia Africa Europe-1 (AAE-1)', 3210, 9485), ('Asia Africa Europe-1 (AAE-1)', 4177, 3160), ('Asia Africa Europe-1 (AAE-1)', 6013, 3160), ('Asia Africa Europe-1 (AAE-1)', 9486, 3259), ('Asia Africa Europe-1 (AAE-1)', 3212, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4265), ('Asia Africa Europe-1 (AAE-1)', 9486, 5962), ('Asia Africa Europe-1 (AAE-1)', 3160, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 9485)]},
				'tag': 'Egypt-cut-failure-segments-weighted'
			}
			
			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country_weighted, total_stats_weighted, country_stats_aggregated_weighted, country_or_as=1, weighted=True)

			event_updates = {
				'type': 'event',
				'name': None,
				'baseline': None,
				'search_radius': None,
				'additional_threshold': None,
				'scope': {'landing_points': ['Al Hudaydah, Yemen']},
				'tag': 'Al-Hudaydah-failure-weighted'
			}
			
			event.update(event_updates)

			_, event_stats = singular_event_impact_analysis (event, cable_segment_against_country_weighted, total_stats_weighted, country_stats_aggregated_weighted, country_or_as=1, weighted=True)

	# The following flag will be set when running experiments corresponding to sections 6 (and Appendix B.4) -- (sensitivity analysis) 
	sensitivity_analysis = False 

	if sensitivity_analysis:

		# Let's generate the concise representations for all levels of inaccuracies accounted. If there is some error, since we use random selection, we will run the experiment for 10 times and average the results 
		accuracies_list = [[1, 0, 0], [0.9, 0.05, 0.05], [0.85, 0.1, 0.05], [0.77, 0.19, 0.04], [0.6, 0.3, 0.1], [0.8, 0, 0.2], [0.7, 0, 0.3], [0.6, 0, 0.4]]
		
		arguments = []
		combination = []
		for accuracy in accuracies_list:
			if accuracy != [1, 0, 0]:
				for _ in range(10):
					arguments.append((conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, accuracy))
					combination.append(accuracy)


		p = multiprocessing.Pool(processes=10)
		results = p.starmap(sensitivity_analysis_helper, tqdm(arguments, total=len(arguments)))
		p.close()
		p.join()

		concise_representations_arguments = {}

		concise_representations_arguments[(1, 0, 0)] = [cable_segment_against_country]

		for index, result in enumerate(results):
			accuracy = combination[index]
			current = concise_representations_arguments.get(tuple(accuracy), [])
			current.append(result)
			concise_representations_arguments[tuple(accuracy)] = current

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['tw', 'ph']},
			'tag': 'TW-PH-earthquake'
		}

		event.update(event_updates)

		# In the process_event_ranges, we internally call the "process_single_event" function which first identifies the impacted cable segments and then return the releavant results as needed by each experiment
		tw_ph_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (tw_ph_stats, 'plots/tw_ph_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['jp']},
			'tag': 'Japan-earthquake'
		}

		event.update(event_updates)

		jp_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (jp_stats, 'plots/japan_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'regions': ['Washington', 'Oregon', 'British Columbia']},
			'tag': 'PNW-earthquake'
		}

		event.update(event_updates)

		pnw_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (pnw_stats, 'plots/pnw_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['id']},
			'tag': 'Indonesia-earthquake'
		}
		
		event.update(event_updates)

		id_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (id_stats, 'plots/indonesia_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['cu', 'jm', 'ht', 'do', 'bs']},
			'tag': 'Carribbean-Hurricane'
		}
		
		event.update(event_updates)

		crb_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (crb_stats, 'plots/carribbean_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['gb', 'ie']},
			'tag': 'British-Isles-Hurricane'
		}
		
		event.update(event_updates)

		bis_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (bis_stats, 'plots/british_isles_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'country_codes': ['bz', 'hn', 'ni', 'pa', 'cr']},
			'tag': 'Central-America-Hurricane'
		}
		
		event.update(event_updates)

		cam_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (cam_stats, 'plots/central_america_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster',
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'additional_threshold': {'special': 0},
			'scope': {'regions': ['Florida', 'Louisiana', 'Texas']},
			'tag': 'Florida-Hurricane'
		}
		
		event.update(event_updates)

		fl_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (fl_stats, 'plots/florida_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'type': 'disaster', 
			'scope': None,
			'additional_threshold': {'special': 0}
		}

		event.update(event_updates)

		# Next, we do each disaster (earthquake, hurricane, sea rise and solar storm)

		event_updates = {
			'name': 'earthquake',
			'baseline': 6,
			'search_radius': 10,
			'tag': 'earthquake'
		}

		event.update(event_updates)

		earthquake_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (earthquake_stats, 'plots/earthquake_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'name': 'hurricane',
			'baseline': 60,
			'search_radius': 50,
			'tag': 'hurricane'
		}

		event.update(event_updates)

		hurricane_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (hurricane_stats, 'plots/hurricane_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'name': 'searise',
			'baseline': 1,
			'search_radius': 10,
			'tag': 'searise'
		}

		event.update(event_updates)

		searise_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (searise_stats, 'plots/searise_inaccuracies_stats', len(accuracies_list))

		event_updates = {
			'name': 'solarstorm',
			'baseline': 50,
			'search_radius': 1,
			'tag': 'solarstorm'
		}

		event.update(event_updates)

		solarstorm_stats = process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats)
		plot_inaccuracies_accounted_maximal_impact (solarstorm_stats, 'plots/solarstorm_inaccuracies_stats', len(accuracies_list))
