import pycountry, reverse_geocode

def generate_cable_segment_to_country_as_maps (cable_segment_to_all_info_map, all_ases):

	ip_links_indices = {}
	ips_indices = {}
	as_links_indices = {}
	as_indices = {}
	as_link_index_to_intra_inter = {}
	ip_link_index_to_intra_inter = {}

	all_countries = [country.alpha_2 for country in pycountry.countries] + ['XK']
	all_countries = set(all_countries)
	ases = list(all_ases)

	cable_segment_against_country = {}
	cable_segment_against_as = {}

	for segment, values in cable_segment_to_all_info_map.items():

		segment_country_result = {}
		segment_as_result = {}

		ip_links = values[0]
		as_links = values[1]
		geolocations = values[2]
		if len(values) == 5:
			scores = values[4]
		else:
			scores = [1] * len(ip_links)

		for ip_link, as_link, geolocation, score in zip(ip_links, as_links, geolocations, scores):

			ip_link_index = ip_links_indices.get(ip_link, len(ip_links_indices))
			if ip_link_index == len(ip_links_indices):
				# Checking if the rare reverse IP-link is present
				ip_link_index = ip_links_indices.get(ip_link[::-1], len(ip_links_indices))
				if ip_link_index == len(ip_links_indices):
					ip_links_indices[ip_link] = ip_link_index

			left_ip_index = ips_indices.get(ip_link[0], len(ips_indices))
			if left_ip_index == len(ips_indices):
				ips_indices[ip_link[0]] = left_ip_index

			right_ip_index = ips_indices.get(ip_link[1], len(ips_indices))
			if right_ip_index == len(ips_indices):
				ips_indices[ip_link[1]] = right_ip_index

			as_link_index = as_links_indices.get(as_link, len(as_links_indices))
			if as_link_index == len(as_links_indices):
				# Checking if reverse as-link is present
				as_link_index = as_links_indices.get(as_link[::-1], len(as_links_indices))
				if as_link_index == len(as_links_indices):
					as_links_indices[as_link] = as_link_index

			left_as_index = as_indices.get(as_link[0], len(as_indices))
			if left_as_index == len(as_indices):
				as_indices[as_link[0]] = left_as_index

			right_as_index = as_indices.get(as_link[1], len(as_indices))
			if right_as_index == len(as_indices):
				as_indices[as_link[1]] = right_as_index

			# If we have an intra-AS link, we assign a tag of 1, else 0
			if as_link[0] == as_link[1]:
				as_link_index_to_intra_inter[as_link_index] = 1
				ip_link_index_to_intra_inter[ip_link_index] = 1
			else:
				as_link_index_to_intra_inter[as_link_index] = 0
				ip_link_index_to_intra_inter[ip_link_index] = 0


			countries = [reverse_geocode.search([i])[0]['country_code'] for i in geolocation if i != '+']

			# Note that even if there are duplicate entries for cases with same countries on both endpoints or intra-AS, when we do analysis, we will do set operation, so it doesn't matter

			for country in countries:
				# The 5 entries are IP link, IPs, AS link, ASes indices, scores (if applicable)
				current = segment_country_result.get(country, [[], [], [], [], []])
				current[0].append(ip_link_index)
				current[1].extend([left_ip_index, right_ip_index])
				current[2].append(as_link_index)
				current[3].extend([left_as_index, right_as_index])
				current[4].append(score)
				segment_country_result[country] = current
			
			
			for AS in as_link:
				# The 5 entries are IP link, IPs, AS link, ASes indices, intra-AS (1 if yes, 0 else), score (if applicable)
				current = segment_as_result.get(AS, [[], [], [], [], []])
				current[0].append(ip_link_index)
				current[1].extend([left_ip_index, right_ip_index])
				current[2].append(as_link_index)
				current[3].extend([left_as_index, right_as_index])
				current[4].append(score)
				segment_as_result[AS] = current	

		cable_segment_against_country[segment] = [segment_country_result.get(country, [[], [], [], [], []]) for country in all_countries]
		cable_segment_against_as[segment] = [segment_as_result.get(AS, [[], [], [], [], []]) for AS in ases]

	metadata = {'IP links': ip_links_indices, 'IPs': ips_indices, 'AS links': as_links_indices, 'ASes': as_indices, 'intra-inter': as_link_index_to_intra_inter, 'intra-inter-ip': ip_link_index_to_intra_inter}

	print (f'Cable segment against country length: {len(cable_segment_against_country)}. Single element length is {len(cable_segment_against_country[list(cable_segment_against_country.keys())[-1]])}. Countries length is {len(all_countries)}')

	return all_countries, ases, cable_segment_against_country, cable_segment_against_as, metadata
