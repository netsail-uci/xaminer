def load_dataset (baseline_threshold=None, log=False):

	with open('datasets/disaster_data/hurricane/hurricanes1.csv') as fp:
		contents = fp.readlines()

	latlon_to_all_knots = {}

	for row in contents:
		
		row = row.split(',')
		lat, lon, speed = float(row[0].strip()), float(row[1].strip()), int(row[2].strip())

		# Due to some incorrect entries in the data source, the following changes are done
		if lat > 90:
			lat, lon = lon, lat 
		if lat < -90:
			lat, lon = lon, lat 

		if speed == -999:
			speed = 0 

		current = latlon_to_all_knots.get((lat, lon), [])
		current.append(speed)
		latlon_to_all_knots[(lat, lon)] = current

	latlon_to_max_knots = {latlon: max(value) for latlon, value in latlon_to_all_knots.items()}

	if baseline_threshold:
		latlon_to_max_knots = {latlon: value for latlon, value in latlon_to_max_knots.items() if value >= baseline_threshold}

	if log:
		print (f'Max knots seen -> {max(latlon_to_max_knots.values())} and Min knots seen -> {min(latlon_to_max_knots.values())}')

	return latlon_to_max_knots, latlon_to_all_knots

if __name__ == '__main__':

	latlon_to_max_knots, latlon_to_all_knots = load_dataset()
