from sklearn.cluster import DBSCAN
import pickle, sys  
from pathlib import Path  


def get_cluster_as_list (clusters, original_list):

	return_list = []
	start = 0
	for index, value in enumerate(clusters):
	    if start == value:
	        return_list.append([])
	        start += 1
	    return_list[value].append(original_list[index])
	len_list = [len(item)/len(clusters) for item in return_list]
	return return_list, len_list


def cluster_locations(locations_list):
    
    cluster = DBSCAN(eps=50/6371., min_samples = 1, algorithm='ball_tree', metric='haversine', leaf_size=2).fit_predict(np.radians(locations_list))
    return get_cluster_as_list(cluster, locations_list)


def get_sorted_mean_clusters (cluster):
    
    return [(list(item[0]), item[1]) for item in list(map(lambda p: (np.mean(p, axis=0), len(p)), sorted(cluster, key=len, reverse=True)))]


def get_best_geolocation_for_all_ips (ip_version=4, sol_threshold=0.05):

	best_geolocation_file = 'datasets/cross_layer_data/geolocation/best_geolocation_v{}'.format(ip_version)

	if Path(best_geolocation_file).exists():
		with open(best_geolocation_file, 'rb') as fp:
			ip_to_best_geolocation = pickle.load(fp)

	else:
		sol_validated_file = 'datasets/cross_layer_data/geolocation/all_validated_ip_location_v{}'.format(ip_version)
		ip_to_best_geolocation = {}

		if Path(sol_validated_file).exists():
			with open(sol_validated_file, 'rb') as fp:
				sol_validated_results = pickle.load(fp)

			for ip, results in sol_validated_results.items():
				location_index = results['location_index']
				coordinates = results['coordinates']
				penalty_counts = results['penalty_count']
				total_counts = results['total_count']

				ip_result = []
				for index, total_count in enumerate(total_counts):
					if total_count > 0:
						coordinate_index = location_index.index(index)
						if penalty_counts[index] / total_count <= sol_threshold:
							ip_result.append(coordinates[coordinate_index])

				if len(ip_result) > 0:
					clusters, len_list = cluster_locations(ip_result)
					mean_cluster = get_sorted_mean_clusters(clusters)
					max_len = max([item[1] for item in mean_cluster])
					max_cluster_locations = [item[0] for item in mean_cluster if item[1] == max_len]
					# Let's just take the first one as the best guess if two have same no of sources agreeing
					ip_to_best_geolocation[ip] = max_cluster_locations[0]

			if len(ip_to_best_geolocation) > 0:
				with open(best_geolocation_file, 'wb') as fp:
					pickle.dump(ip_to_best_geolocation, fp)

		else:
			print ('Validated geolocation file not generated/found. If using Nautilus geolocation, download the latest file from "https://gitlab.com/netsail-uci/nautilus/-/tree/main/results/geolocation?ref_type=heads" and store them in datasets/cross_layer_data/geolocation folder')
			sys.exit(1)

	return ip_to_best_geolocation
