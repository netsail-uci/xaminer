import pickle, random
from pathlib import Path  

def load_nautilus_mapping (ip_version=4, condition=None, log=False):

	if not condition:
		condition = 'de_te'

	nautilus_mapping_file = 'datasets/cross_layer_data/link_to_cable_and_score_mapping_sol_validated_v{}'.format(ip_version)

	if not Path(nautilus_mapping_file).exists():
		print (f'Download the latest Nautilus mapping (link_to_cable_and_score_mapping_sol_validated_v. file) from "https://gitlab.com/netsail-uci/nautilus/-/tree/main/results?ref_type=heads" and place that in datasets/cross_layer_data/ folder')

	with open(nautilus_mapping_file, 'rb') as fp:
		mapping = pickle.load(fp)

	# Get the Nautilus mapped links that have atleast 1 mapping and belong to the given category condition
	conditioned_mapping = {k: v for k,v in mapping.items() if ((condition == 'de_te' and v[-1] != 'de_te') or (condition in v[-1])) and len(v[1]) > 0}

	if log:
		print (f'Nautilus mapping had {len(mapping)} links, but with the imposed condition we have mapping for {len(conditioned_mapping)} links')

	return conditioned_mapping


def generate_cable_segments_to_all_info_map (conditioned_mapping, ip_to_asn_map, ip_to_best_geolocation):

	cable_segment_to_all_info_map = {}

	# For simplicity, we are just going to take the top cable and landing point prediction for each link
	# Also to note that cable_segments are going to have the landing point ID tuples from TeleGeography (as nautilus retained those IDs)

	for ip_link, mapping_contents in conditioned_mapping.items():

		# First check if nautilus has generated any cable mapping
		if len(mapping_contents[3]) > 0:

			best_landing_points = mapping_contents[3][0][0]
			best_cable = mapping_contents[1][0]
			category = mapping_contents[-1]

			geolocation = (ip_to_best_geolocation.get(ip_link[0], '+'), ip_to_best_geolocation.get(ip_link[1], '+'))
			asn_link = (ip_to_asn_map.get(ip_link[0], '+'), ip_to_asn_map.get(ip_link[1], '+'))

			cable_segment = (best_cable, best_landing_points[0], best_landing_points[1])

			# The order we store the info : 0 -> ip-links, 1 -> as-links, 2 -> geolocations, 3 -> categories
			current = cable_segment_to_all_info_map.get(cable_segment, [[], [], [], []])

			# If there are no entries for this landing points direction, let's try the reverse
			if len(current[0]) == 0:
				cable_segment_rev = (best_cable, best_landing_points[1], best_landing_points[0])
				current_rev = cable_segment_to_all_info_map.get(cable_segment_rev, [[], [], [], []])
				
				if len(current_rev[0]) > 0:
					current_rev[0].append(ip_link[::-1])
					current_rev[1].append(asn_link[::-1])
					current_rev[2].append(geolocation[::-1])
					current_rev[3].append(category)

					cable_segment_to_all_info_map[cable_segment_rev] = current_rev
				
				# Looks like even there is no match for the reverse as well				
				else:
					current[0].append(ip_link)
					current[1].append(asn_link)
					current[2].append(geolocation)
					current[3].append(category)

					cable_segment_to_all_info_map[cable_segment] = current

			# If there is already a match with the current one, just got with it
			else:

				current[0].append(ip_link)
				current[1].append(asn_link)
				current[2].append(geolocation)
				current[3].append(category)

				cable_segment_to_all_info_map[cable_segment] = current

	return cable_segment_to_all_info_map


def generate_cable_segments_to_all_info_map_all_cable_top_segments (conditioned_mapping, ip_to_asn_map, ip_to_best_geolocation):

	cable_segment_to_all_info_map = {}

	# For this version, we will use all cables with the best landing points (ie., best segment) for each cable
	# Also to note that cable_segments are going to have the landing point ID tuples from TeleGeography (as nautilus retained those IDs)

	for ip_link, mapping_contents in conditioned_mapping.items():

		# First check if nautilus has generated any cable mapping
		if len(mapping_contents[3]) > 0:

			geolocation = (ip_to_best_geolocation.get(ip_link[0], '+'), ip_to_best_geolocation.get(ip_link[1], '+'))
			asn_link = (ip_to_asn_map.get(ip_link[0], '+'), ip_to_asn_map.get(ip_link[1], '+'))
			category = mapping_contents[-1]
			score = 1 / len(mapping_contents[1])

			for index, best_cable in enumerate(mapping_contents[1]):
				best_landing_points = mapping_contents[3][index][0]

				cable_segment = (best_cable, best_landing_points[0], best_landing_points[1])

				# The order we store the info : 0 -> ip-links, 1 -> as-links, 2 -> geolocations, 3 -> categories
				current = cable_segment_to_all_info_map.get(cable_segment, [[], [], [], [], []])

				# If there are no entries for this landing points direction, let's try the reverse
				if len(current[0]) == 0:
					cable_segment_rev = (best_cable, best_landing_points[1], best_landing_points[0])
					current_rev = cable_segment_to_all_info_map.get(cable_segment_rev, [[], [], [], [], []])
					
					if len(current_rev[0]) > 0:
						current_rev[0].append(ip_link[::-1])
						current_rev[1].append(asn_link[::-1])
						current_rev[2].append(geolocation[::-1])
						current_rev[3].append(category)
						current_rev[4].append(score)

						cable_segment_to_all_info_map[cable_segment_rev] = current_rev
					
					# Looks like even there is no match for the reverse as well				
					else:
						current[0].append(ip_link)
						current[1].append(asn_link)
						current[2].append(geolocation)
						current[3].append(category)
						current[4].append(score)

						cable_segment_to_all_info_map[cable_segment] = current

				# If there is already a match with the current one, just got with it
				else:

					current[0].append(ip_link)
					current[1].append(asn_link)
					current[2].append(geolocation)
					current[3].append(category)
					current[4].append(score)

					cable_segment_to_all_info_map[cable_segment] = current

	return cable_segment_to_all_info_map


def generate_cable_segments_to_all_info_map_random (conditioned_mapping, ip_to_asn_map, ip_to_best_geolocation, accuracy=(1, 0, 0)):

	cable_segment_to_all_info_map = {}

	correct_num = round(len(conditioned_mapping) * accuracy[0])
	incorrect_num = round(len(conditioned_mapping) * accuracy[1])
	skip_num = len(conditioned_mapping) - correct_num - incorrect_num

	all_indices = [index for index, mapping_contents in enumerate(conditioned_mapping.values()) if len(mapping_contents[3]) > 0]
	correct_indices = sorted(random.sample(all_indices, correct_num))
	leftover = list(set(all_indices) - set(correct_indices))
	incorrect_indices = sorted(random.sample(leftover, incorrect_num))
	skip_indices = sorted(set(leftover) - set(incorrect_indices))

	# For this version, we will use all cables except top and select a cable segment at random
	# Also to note that cable_segments are going to have the landing point ID tuples from TeleGeography (as nautilus retained those IDs)

	for index, (ip_link, mapping_contents) in enumerate(conditioned_mapping.items()):

		# First check if nautilus has generated any cable mapping
		if len(mapping_contents[3]) > 0:

			# If index falls under the correct index category, it means Nautilus made a correct mapping for this IP link
			if len(correct_indices) > 0 and index == correct_indices[0]:

				correct_indices.pop(0)

				best_landing_points = mapping_contents[3][0][0]
				best_cable = mapping_contents[1][0]
				category = mapping_contents[-1]

				geolocation = (ip_to_best_geolocation.get(ip_link[0], '+'), ip_to_best_geolocation.get(ip_link[1], '+'))
				asn_link = (ip_to_asn_map.get(ip_link[0], '+'), ip_to_asn_map.get(ip_link[1], '+'))

				cable_segment = (best_cable, best_landing_points[0], best_landing_points[1])

				# The order we store the info : 0 -> ip-links, 1 -> as-links, 2 -> geolocations, 3 -> categories
				current = cable_segment_to_all_info_map.get(cable_segment, [[], [], [], []])

				# If there are no entries for this landing points direction, let's try the reverse
				if len(current[0]) == 0:
					cable_segment_rev = (best_cable, best_landing_points[1], best_landing_points[0])
					current_rev = cable_segment_to_all_info_map.get(cable_segment_rev, [[], [], [], []])
					
					if len(current_rev[0]) > 0:
						current_rev[0].append(ip_link[::-1])
						current_rev[1].append(asn_link[::-1])
						current_rev[2].append(geolocation[::-1])
						current_rev[3].append(category)

						cable_segment_to_all_info_map[cable_segment_rev] = current_rev
					
					# Looks like even there is no match for the reverse as well				
					else:
						current[0].append(ip_link)
						current[1].append(asn_link)
						current[2].append(geolocation)
						current[3].append(category)

						cable_segment_to_all_info_map[cable_segment] = current

				# If there is already a match with the current one, just got with it
				else:

					current[0].append(ip_link)
					current[1].append(asn_link)
					current[2].append(geolocation)
					current[3].append(category)

					cable_segment_to_all_info_map[cable_segment] = current

			# If index falls under the incorrect index category, it means Nautilus made a mapping for this IP link correctly, but it was not Nautilus' top choice, so we pick a non-top one at random as the correct ground truth
			if len(incorrect_indices) > 0 and index == incorrect_indices[0]:

				incorrect_indices.pop(0)

				geolocation = (ip_to_best_geolocation.get(ip_link[0], '+'), ip_to_best_geolocation.get(ip_link[1], '+'))
				asn_link = (ip_to_asn_map.get(ip_link[0], '+'), ip_to_asn_map.get(ip_link[1], '+'))
				category = mapping_contents[-1]
				
				all_cable_segments = []

				# In this we don't want the top cable segment predicted, but something else
				for index, cable in enumerate(mapping_contents[1]):
					for i, landing_point in enumerate(mapping_contents[3][index]):
						if index == 0 and i == 0:
							continue
						else:
							all_cable_segments.append((cable, landing_point[0], landing_point[1]))

				if len(all_cable_segments) > 0:
				
					cable_segment = random.choice(all_cable_segments)

					# The order we store the info : 0 -> ip-links, 1 -> as-links, 2 -> geolocations, 3 -> categories
					current = cable_segment_to_all_info_map.get(cable_segment, [[], [], [], [], []])

					# If there are no entries for this landing points direction, let's try the reverse
					if len(current[0]) == 0:
						cable_segment_rev = (cable_segment[0], cable_segment[2], cable_segment[1])
						current_rev = cable_segment_to_all_info_map.get(cable_segment_rev, [[], [], [], [], []])
						
						if len(current_rev[0]) > 0:
							current_rev[0].append(ip_link[::-1])
							current_rev[1].append(asn_link[::-1])
							current_rev[2].append(geolocation[::-1])
							current_rev[3].append(category)

							cable_segment_to_all_info_map[cable_segment_rev] = current_rev
						
						# Looks like even there is no match for the reverse as well				
						else:
							current[0].append(ip_link)
							current[1].append(asn_link)
							current[2].append(geolocation)
							current[3].append(category)

							cable_segment_to_all_info_map[cable_segment] = current

					# If there is already a match with the current one, just got with it
					else:

						current[0].append(ip_link)
						current[1].append(asn_link)
						current[2].append(geolocation)
						current[3].append(category)

						cable_segment_to_all_info_map[cable_segment] = current

			# If index falls under the skip index category, Nautilus didn't make correct mapping at all for this IP link
			if len(skip_indices) > 0 and index == skip_indices[0]:

				skip_indices.pop(0)

	return cable_segment_to_all_info_map

