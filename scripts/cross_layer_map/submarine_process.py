from pathlib import Path
import pickle, sys, math
import country_converter as coco
from sklearn.neighbors import BallTree
from geopy.geocoders import Nominatim

from collections import namedtuple, Counter
LandingPoints = namedtuple('LandingPoints', ['latitude', 'longitude', 'country', 'location', 'cable'])

def load_landing_points_dict ():

	file_path = Path('datasets/cross_layer_data/submarine_data/landing_points_dict')

	if file_path.exists():
		with open(file_path, 'rb') as fp:
			landing_points = pickle.load(fp)

		return landing_points

	else:
		print ('Landing point dictionary not found/generated')
		sys.exit(1)


def get_lp_id_to_country_dict (landing_points):

	cc = coco.CountryConverter()

	lp_id_country_dict = {k: cc.convert(names=[v.country], to='ISO2') for k,v in landing_points.items()}
	country_to_lp_id_dict = {}
	for lp_id, country in lp_id_country_dict.items():
		current = country_to_lp_id_dict.get(country, [])
		current.append(lp_id)
		country_to_lp_id_dict[country] = current 

	return lp_id_country_dict, country_to_lp_id_dict


def convert_degree_to_radians (item):
	return tuple(map(math.radians, item))


def construct_balltree (landing_points):

	# lp_id indicates a landing point ID found from submarine cable map data in TeleGeography
	latlon_to_lp_id = {(v.latitude, v.longitude): k for k,v in landing_points.items()}
	latlon_in_radians = list(map(convert_degree_to_radians, latlon_to_lp_id.keys()))
	tree = BallTree(latlon_in_radians, metric = "haversine", leaf_size=2)
	return latlon_to_lp_id, tree 


def list_conversion_from_array (array):
	return [i.tolist() for i in array]


def find_closest_lp_for_given_latlons (latlon_to_lp_id, tree, loc, num=None, radius=None):

	loc_in_radians = list(map(convert_degree_to_radians, [loc]))

	if (num == None and radius == None) or (num and radius):
		print (f'Either give valid number for nearest neighbor search or give the radius to search, not none or both')
		sys.exit(1)

	else:
		if num:
			dist, ind = map(list_conversion_from_array, tree.query(loc_in_radians, k=num))
		else:
			ind, dist = map(list_conversion_from_array, tree.query_radius(loc_in_radians, radius/6371, return_distance=True, sort_results=True))

		landing_points = []
		landing_point_ids = []

		for i in range(len(ind[0])):
			landing_point_lat_lon = list(latlon_to_lp_id.keys())[ind[0][i]]
			landing_points.append(landing_point_lat_lon)
			landing_point_ids.append(latlon_to_lp_id[landing_point_lat_lon])

	return dist, landing_points, landing_point_ids


def identify_reverse_geolocation_information_for_all_landing_points (latlon_to_lp_id, log=False):

	file_path = Path('datasets/cross_layer_data/submarine_data/landing_point_locations')

	if file_path.exists():
		with open(file_path, 'rb') as fp:
			lat_lon_reverse_geolocation_result = pickle.load(fp)

	else:
		geolocator = Nominatim(user_agent="test")
		lat_lon_reverse_geolocation_result = {}

		for count, latlon in enumerate(latlon_to_lp_id.keys()):
			try:
				lat_lon_reverse_geolocation_result[latlon] = geolocator.reverse(latlon, language='en').raw
				if count % 50 == 0 and log:
					print (f'Processed {count} locations')
			except:
				continue

		with open(file_path, 'wb') as fp:
			pickle.dump(lat_lon_reverse_geolocation_result, fp)

	return lat_lon_reverse_geolocation_result

