from cross_layer_map.submarine_process import load_landing_points_dict, construct_balltree, identify_reverse_geolocation_information_for_all_landing_points, find_closest_lp_for_given_latlons
import random
import numpy as np 

def get_landing_points_given_constraints (latlon_to_value, radius=10, region=None, additional_thresholds=None, log=None):

	'''
	Inputs
		latlon_to_value -> The default structure of a disaster model which maps the lat-lon tuple to the disaster specific value
		radius -> The zone around a given lat-lon tuple to consider to check if any landing points falls under it, default value is 10 km
		region -> region to identify landing points over
			-> If None, it indicates we consider the entire world 
			-> key -> landing_points -> a list of all landing points (not the IDs) -> should be the string name as in TeleGeography -> If used, no other parameters should be set as we just return after this
			-> key -> countries -> a list of all countries to consider
			-> key -> country_codes -> a list of all country codes to consider 
			-> key -> regions -> a list of sub-divisions within a country to consider
		additional_thresholds -> additional filter based on value, defaults to None
			-> keys -> min, max, special (this can be used to eliminate stuff like 0, [], ..)
			-> more keys -> retain (fraction of latlons to retain), choice (can be random, top, prob_top)

	Outputs
		filter_lp_id_list -> A list of all identified landing points satisfying the constraints
	'''

	landing_points = load_landing_points_dict()

	filter_lp_id_list = []

	# This is straight forward, map the name to lp id and return 

	if region and 'landing_points' in region:
		landing_point_location_to_lp_id = {v.location: k for k, v in landing_points.items()}
		for item in region['landing_points']:
			lp_id = landing_point_location_to_lp_id.get(item, None)
			if lp_id:
				filter_lp_id_list.append(lp_id)

		if log:
			print (f'Length of filter lp id list: {len(filter_lp_id_list)}')

		return filter_lp_id_list

	latlon_to_lp_id, tree = construct_balltree(landing_points)
	latlon_rev_geo_result = identify_reverse_geolocation_information_for_all_landing_points(latlon_to_lp_id, log)

	if additional_thresholds:
		latlon_to_value = {latlon: value for latlon, value in latlon_to_value.items() 
							if (('special' in additional_thresholds and value != additional_thresholds['special']) or ('special' not in additional_thresholds)) and
							(('min' in additional_thresholds and value >= additional_thresholds['min']) or ('min' not in additional_thresholds)) and 
							(('max' in additional_thresholds and value <= additional_thresholds['max']) or ('max' not in additional_thresholds))} 

	if log:
		print (f'After applying additional thresholds, the length of latlon to value is {len(latlon_to_value)}')


	# Now comes the region selection part

	# We will use this to eliminate re-examining for the same landing point
	examined_latlons = []
	filter_lp_id_list_temp = []
	sub_dis_latlon_to_lp_ids_list = {}

	for count, (dis_latlons, value) in enumerate(latlon_to_value.items()):
		dist, landing_point_lat_lon, landing_point_id = find_closest_lp_for_given_latlons (latlon_to_lp_id, tree, dis_latlons, radius=radius)
		matched_lp_ids = []

		for index, sub_latlon in enumerate(landing_point_lat_lon):
			if sub_latlon not in examined_latlons:
				rev_geo = latlon_rev_geo_result.get(sub_latlon, None)

				# If not region (ie., global), no additional checks needed
				# If region is given , check either country code or country or regions match
				if rev_geo and 'country_code' in rev_geo['address'] and 'country' in rev_geo['address']:
					if (not region) or \
						(region and ((('country_codes' in region) and (any(item.lower() == rev_geo['address']['country_code'].lower() for item in region['country_codes']))) or 
						   (('countries' in region) and (any(item.lower() == rev_geo['address']['country'].lower() for item in region['countries']))) or 
						   (('regions' in region) and (any(item.lower() in [i.lower() for i in rev_geo['address'].values()] for item in region['regions']))))):

						filter_lp_id_list_temp.append(landing_point_id[index])
						matched_lp_ids.append(landing_point_id[index])

				examined_latlons.append(sub_latlon)	
			
			else:
				if landing_point_id[index] in filter_lp_id_list_temp:
					matched_lp_ids.append(landing_point_id[index])

		if len(matched_lp_ids) > 0:
			sub_dis_latlon_to_lp_ids_list[dis_latlons] = matched_lp_ids


	if 'retain' in additional_thresholds and 'choice' in additional_thresholds:
		num_to_pick = int(len(sub_dis_latlon_to_lp_ids_list) * additional_thresholds['retain'])
		sub_dis_latlon_to_values = {k: latlon_to_value[k] for k in sub_dis_latlon_to_lp_ids_list}
		sub_dis_latlon_to_values_sorted = dict(sorted(sub_dis_latlon_to_values.items(), key=lambda item: item[1], reverse=True))

		if additional_thresholds['choice'] == 'random':
			# In this we just want to select n random elements from the disaster latlons
			selected_dis_latlons = random.sample(list(sub_dis_latlon_to_lp_ids_list.keys()), num_to_pick)
		elif additional_thresholds['choice'] == 'top':
			# Here we pick the disaster latlons with top n% of values
			selected_dis_latlons = list(sub_dis_latlon_to_values_sorted.keys())[:num_to_pick]
		elif additional_thresholds['choice'] == 'prob_top':
			# Here we mix random with the value, which implies higher value means more chance of getting pickled
			total_value_sum = sum(sub_dis_latlon_to_values_sorted.values())
			probabilities = [value / total_value_sum for value in sub_dis_latlon_to_values_sorted.values()]
			# Choices seems to have in-built replacement, so let's get one by one and check
			# selected_dis_latlons = random.choices(list(sub_dis_latlon_to_values_sorted.keys()), weights=probabilities, k=num_to_pick)
			# So instead we use numpy random choice
			keys = list(sub_dis_latlon_to_values_sorted.keys())
			selected_dis_latlons_index_array = np.random.choice(len(keys), size=num_to_pick, replace=False, p=probabilities)
			selected_dis_latlons = [keys[i] for i in selected_dis_latlons_index_array]
		elif additional_thresholds['choice'] == 'prob_top_rev':
			# Here we mix random with the value, which implies lower value means more chance of getting pickled
			total_value_sum = sum(sub_dis_latlon_to_values_sorted.values())
			probabilities = [1.0 - (value / total_value_sum) for value in sub_dis_latlon_to_values_sorted.values()]
			sum_probabilities = sum(probabilities)
			probabilities = [p / sum_probabilities for p in probabilities]
			keys = list(sub_dis_latlon_to_values_sorted.keys())
			selected_dis_latlons_index_array = np.random.choice(len(keys), size=num_to_pick, replace=False, p=probabilities)
			selected_dis_latlons = [keys[i] for i in selected_dis_latlons_index_array]
		else:
			print ('No valid choice presented, we will select at random by default')
			selected_dis_latlons = random.choices(list(sub_dis_latlon_to_lp_ids_list.keys()), k=num_to_pick)

		filter_lp_id_set = set()
		for dis_latlons in list(selected_dis_latlons):
			filter_lp_id_set.update(sub_dis_latlon_to_lp_ids_list[dis_latlons])
		filter_lp_id_list = list(filter_lp_id_set)


	else:

		filter_lp_id_list = filter_lp_id_list_temp

	return filter_lp_id_list


def get_impacted_segments_given_constraints (cable_segment_to_all_info_map, latlon_to_value=None, radius=10, region=None, additional_thresholds=None, count=0, log=None):

	all_matched_segments = set()

	# Use the "cable_segment_partial" if a particular cable and landing station is known to have been impacted, but the other segment end could be anything
	if region and 'cable_segment_partial' in region:

		landing_points = load_landing_points_dict()
		landing_point_location_to_lp_id = {v.location: k for k, v in landing_points.items()}
		lp_id = landing_point_location_to_lp_id.get(region['cable_segment_partial'][1], None)

		# Check if we have a valid landing point ID for the given landing point
		if lp_id:
			csp = (cable, lp_id)
			matching_segments = [item for item in cable_segment_to_all_info_map if len(set(csp) & set(item)) == 2]
			all_matched_segments.update(set(matching_segments))
		else:
			print (f'Invalid landing point provided')

	# Use the "cable_segment" option to select strictly 1 cable segment, else we will get error
	if region and 'cable_segment' in region:

		matching_segment = [item for item in cable_segment_to_all_info_map if set(region['cable_segment']) == set(item)]
		all_matched_segments.update(set(matching_segment))

		if len(matching_segment) == 2:
			print ('Something went wrong and we got 2 cable segment matches, check if cable_segment_map is correctly generated')

		elif len(matching_segment) != 1:
			print (f'No match found for given cable segment: {region["cable_segment"]}')

	# Use the "cable_segments" option to select multiple cable segments at once
	if region and 'cable_segments' in region:

		matching_segments = [item for item in cable_segment_to_all_info_map if any([set(item) == set(cable_segment) for cable_segment in region['cable_segments']])]
		all_matched_segments.update(set(matching_segments))

		if len(matching_segments) == 0:
			print (f'No match found for cable: {region["cable"]}')

	# Use the "cable" option to select any cable that is completely impacted (ie., all segments are impacted)
	if region and 'cable' in region:
		matching_segments = [item for item in cable_segment_to_all_info_map if item[0] in cable]
		all_matched_segments.update(set(matching_segments))

		if len(matching_segments) == 0:
			print (f'No match found for cable: {region["cable"]}')

	# If there are specific regions (countries or sub-divisions), landing points or no region provided, the following code is run, where we use "get_landing_points_given_constraints" function to get impacted landing points and construct the impacted cable segments
	
	if (not region) or (len(set(['landing_points', 'country_codes', 'countries', 'regions']) & set(region.keys())) > 0):

		if not latlon_to_value:
			print ('Provide the latlon to value for given event/disaster. Now we are using the default latlon distribution with 0.1 degree tiles')
			# Essentially putting a default value of 1 for all latlon tiles with 0.1 distance
			latlon_to_value = {}
			for latitude in range(-900, 900, 1):
				for longitude in range(-1800, 1800, 1):
					latlon_to_value[(latitude/10, longitude/10)] = 1

		filter_lp_id_list = get_landing_points_given_constraints (latlon_to_value, radius, region, additional_thresholds, log)

		if log:
			print (f'We have {len(filter_lp_id_list)} matches')

		matching_segments = [item for item in cable_segment_to_all_info_map if len(set(item) & set(filter_lp_id_list)) > 0]
		all_matched_segments.update(set(matching_segments))

	if log:
		print (f'Finished processing for {count} combination')

	return all_matched_segments	

