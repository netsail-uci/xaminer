from cross_layer_map.submarine_process import LandingPoints, load_landing_points_dict, get_lp_id_to_country_dict
from itertools import chain
import importlib, pickle, multiprocessing, pycountry, sys
from pathlib import Path
from failed_cable_identification.fci import get_impacted_segments_given_constraints
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster import hierarchy
from copy import deepcopy

def load_country_data():

	countries = [country.alpha_2 for country in pycountry.countries] + ['XK']
	countries = set(countries)
	return countries


def load_ases_data (cable_segment_to_all_info_map=None):

	as_file = Path('results/all_ases_current')

	if as_file.exists():
		with open(as_file, 'rb') as fp:
			ases = pickle.load(fp)
	else:
		if cable_segment_to_all_info_map:
			all_as_links = set(chain(*[item[1] for item in cable_segment_to_all_info_map.values()]))
			ases = set(chain(*[list(item) for item in list(all_as_links)]))

			# Writing to file for future use
			with open(as_file, 'wb') as fp:
				pickle.dump(ases, fp)
		else:
			print (f'Cable segment map is not provided !! Run atleast once with that to enable loading from file for future runs')

	return ases


def get_full_stats_for_each_layer (cable_segment_against_country, metadata, mode=1, return_mode=0):

	all_cable_segments = set(cable_segment_against_country.keys())

	# For all cables, there are two ways to consider
	# (i) Use the cable segments to get all the cables mapped (or)
	# (ii) Use the results from submarine cable map directly to get the cables

	if mode == 0:
		all_cables = set([item[0] for item in all_cable_segments])
	else:
		landing_points = load_landing_points_dict()
		all_cables = set(chain(*[item.cable for item in landing_points.values()]))

	all_ip_links = set(metadata['IP links'].keys())
	all_ips = set(metadata['IPs'].keys())
	all_as_links = set(metadata['AS links'].keys())
	all_ases = set(metadata['ASes'].keys())

	all_data = [all_cable_segments, all_cables, all_ip_links, all_ips, all_as_links, all_ases]
	total_stats = [len(item) for item in all_data]

	if return_mode == 0:
		return total_stats
	elif return_mode == 1:
		return all_data
	else:
		return (total_stats, all_data)


def get_full_stats_aggregated (cable_segment_against_specifics, countries, ases, country_or_as=1, return_mode=0):

	country_stats_aggregated, country_stats_aggregated_data, as_stats_aggregated, as_stats_aggregated_data = {}, {}, {}, {}

	if country_or_as == 1:
		cable_segment_against_country = cable_segment_against_specifics
	elif country_or_as == 2:
		cable_segment_against_as = cable_segment_against_specifics
	else:
		cable_segment_against_country, cable_segment_against_as = cable_segment_against_specifics

	if country_or_as & 1:

		for index, country in enumerate(countries):
			country_impacts = [item[index] for item in cable_segment_against_country.values()]

			segments_impacted = set([list(cable_segment_against_country)[i] for i, content in enumerate(country_impacts) if len(content[0]) > 0])
			cables_impacted = set([item[0] for item in segments_impacted])

			ip_links_impacted, ips_impacted, as_links_impacted, as_impacted, ip_links_weighted_count = [], [], [], [], []
			_ = [(ip_links_impacted.extend(item[0]), ips_impacted.extend(item[1]), as_links_impacted.extend(item[2]), as_impacted.extend(item[3]), ip_links_weighted_count.extend(item[4])) for item in country_impacts]

			ip_links_weighted_count = sum(ip_links_weighted_count)
			
			country_stats_aggregated[country] = [len(item) if index != 3 else ip_links_weighted_count for index, item in enumerate([segments_impacted, cables_impacted, set(ip_links_impacted), set(ips_impacted), set(as_links_impacted), set(as_impacted)])]
			if return_mode == 1:
				country_stats_aggregated_data[country] = [segments_impacted, cables_impacted, set(ip_links_impacted), set(ips_impacted), set(as_links_impacted), set(as_impacted)]


	if country_or_as & 2:

		for index, AS in enumerate(ases):
			as_impacts = [item[index] for item in cable_segment_against_as.values()]

			segments_impacted = set([list(cable_segment_against_as)[i] for i, content in enumerate(as_impacts) if len(content[0]) > 0])
			cables_impacted = set([item[0] for item in segments_impacted])

			ip_links_impacted, ips_impacted, as_links_impacted, as_impacted, ip_links_weighted_count = [], [], [], [], []
			_ = [(ip_links_impacted.extend(item[0]), ips_impacted.extend(item[1]), as_links_impacted.extend(item[2]), as_impacted.extend(item[3]), ip_links_weighted_count.extend(item[4])) for item in as_impacts]

			ip_links_weighted_count = sum(ip_links_weighted_count)

			as_stats_aggregated[AS] = [len(item) if index != 3 else ip_links_weighted_count for index, item in enumerate([segments_impacted, cables_impacted, set(ip_links_impacted), set(ips_impacted), set(as_links_impacted), set(as_impacted)])]
			if return_mode == 1:
				as_stats_aggregated_data[AS] = [segments_impacted, cables_impacted, set(ip_links_impacted), set(ips_impacted), set(as_links_impacted), set(as_impacted)]

	if return_mode == 0:
		return country_stats_aggregated, as_stats_aggregated
	elif return_mode == 1:
		return country_stats_aggregated, as_stats_aggregated, country_stats_aggregated_data, as_stats_aggregated_data


def get_impact_results (impacted_segments, cable_segment_against_country, cable_segment_against_as, country_or_as=1):

	impacted_segments = list(impacted_segments)

	# The overall impacts captures the global impact of the said event (based on the impacted segments identified by the fci module)
	overall_impacted_cables = set([item[0] for item in impacted_segments])
	overall_ip_links_impacted, overall_ips_impacted, overall_as_links_impacted, overall_as_impacted = set(), set(), set(), set()

	# These capture the results at a granularity of each country and AS respectively
	# Each country/AS in this will have 6 entries corresponding to cable segments, cables, IP links, IPs, AS links, ASes respectively
	impact_per_country, impact_per_as = {}, {}

	ip_links_weighted_per_country, ip_links_weighted_per_as = {}, {}

	if country_or_as & 1:

		countries = load_country_data()

		default_value = [[[], [], [], [], []] for _ in countries]

		impacted_cable_segment_against_country = [cable_segment_against_country.get(segment, default_value) for segment in impacted_segments]

		for index, country in enumerate(countries):
			try:
				country_impacts = [item[index] for item in impacted_cable_segment_against_country]
			except:
				print (f'Problem: Impacted segments total: {len(impacted_segments)}, Impacted cable segment against country size: {len(impacted_cable_segment_against_country)}, single entry size is : {len(impacted_cable_segment_against_country[-1])}, current index is {index} and country is {country}')
			
			# If any IP link is found, then we count that segment with the country
			segments_impacted = set([impacted_segments[i] for i, content in enumerate(country_impacts) if len(content[0]) > 0])
			cables_impacted = set([item[0] for item in segments_impacted])

			ip_links_impacted, ips_impacted, as_links_impacted, as_impacted, ip_links_weighted_count = [], [], [], [], []
			_ = [(ip_links_impacted.extend(item[0]), ips_impacted.extend(item[1]), as_links_impacted.extend(item[2]), as_impacted.extend(item[3]), ip_links_weighted_count.extend(item[4])) for item in country_impacts]

			ip_links_impacted = set(ip_links_impacted)
			ips_impacted = set(ips_impacted)
			as_links_impacted = set(as_links_impacted)
			as_impacted = set(as_impacted)

			ip_links_weighted_count = sum(ip_links_weighted_count)

			ip_links_weighted_per_country[country] = ip_links_weighted_count

			impact_per_country[country] = [segments_impacted, cables_impacted, ip_links_impacted, ips_impacted, as_links_impacted, as_impacted]

			overall_ip_links_impacted.update(ip_links_impacted)
			overall_ips_impacted.update(ips_impacted)
			overall_as_links_impacted.update(as_links_impacted)
			overall_as_impacted.update(as_impacted)

	if country_or_as & 2:

		ases = load_ases_data()

		default_value = [[[], [], [], [], []] for _ in ases]

		impacted_cable_segment_against_as = [cable_segment_against_as.get(segment, default_value) for segment in impacted_segments]

		for index, AS in enumerate(ases):
			as_impacts = [item[index] for item in impacted_cable_segment_against_as]

			# If any IP link is found, then we count that segment with the AS
			segments_impacted = set([impacted_segments[i] for i, content in enumerate(as_impacts) if len(content[0]) > 0])
			cables_impacted = set([item[0] for item in segments_impacted])

			ip_links_impacted, ips_impacted, as_links_impacted, as_impacted, ip_links_weighted_count = [], [], [], [], []
			_ = [(ip_links_impacted.extend(item[0]), ips_impacted.extend(item[1]), as_links_impacted.extend(item[2]), as_impacted.extend(item[3]), ip_links_weighted_count.extend(item[4])) for item in as_impacts]

			ip_links_impacted = set(ip_links_impacted)
			ips_impacted = set(ips_impacted)
			as_links_impacted = set(as_links_impacted)
			as_impacted = set(as_impacted)
			ip_links_weighted_count = sum(ip_links_weighted_count)

			ip_links_weighted_per_as[AS] = ip_links_weighted_count

			impact_per_as[AS] = [segments_impacted, cables_impacted, ip_links_impacted, ips_impacted, as_links_impacted, as_impacted]

			overall_ip_links_impacted.update(ip_links_impacted)
			overall_ips_impacted.update(ips_impacted)
			overall_as_links_impacted.update(as_links_impacted)
			overall_as_impacted.update(as_impacted)

	overall_impacts = [set(impacted_segments), overall_impacted_cables, overall_ip_links_impacted, overall_ips_impacted, overall_as_links_impacted, overall_as_impacted]

	return impact_per_country, impact_per_as, overall_impacts, ip_links_weighted_per_country, ip_links_weighted_per_as


def process_single_event (cable_segment_against_specifics, latlon_to_value=None, radius=10, region=None, additional_threshold=None, count=0, country_or_as=1, individual_impacts=False, return_overall_impacts=False, multiple_events=False, weighted=False, log=None):

	# Identifying the impacted cable segments
	# Multiple events flag is set only when we desire the combined effects of multiple events
	if multiple_events:
		impacted_segments = set()
		for lv, rad, reg, at in zip(latlon_to_value, radius, region, additional_threshold):
			event_segments = get_impacted_segments_given_constraints(cable_segment_against_specifics, lv, rad, reg, at, count, log)
			impacted_segments.update(event_segments)
	else:
		impacted_segments = get_impacted_segments_given_constraints(cable_segment_against_specifics, latlon_to_value, radius, region, additional_threshold, count, log)
	
	cable_segment_against_country, cable_segment_against_as = {}, {}

	# Use the following codes to process : 1 -- country only, 2 -- AS only, 3 -- both country and AS (if 3 used, cable segment against specifics should be a tuple of cable_segment_against_country and cable_segment_against_as)
	if country_or_as == 1:
		cable_segment_against_country = cable_segment_against_specifics
	elif country_or_as == 2:
		cable_segment_against_as = cable_segment_against_as
	else:
		cable_segment_against_country, cable_segment_against_as = cable_segment_against_specifics

	impact_per_country, impact_per_as = {}, {}

	if individual_impacts:
		if weighted:
			impact_per_country, impact_per_as, overall_impacts, ip_links_weighted_per_country, ip_links_weighted_per_as = get_impact_results(impacted_segments, cable_segment_against_country, cable_segment_against_as, country_or_as)
		else:
			impact_per_country, impact_per_as, overall_impacts, _, _ = get_impact_results(impacted_segments, cable_segment_against_country, cable_segment_against_as, country_or_as)
	else:
		_, _, overall_impacts, _, _ = get_impact_results(impacted_segments, cable_segment_against_country, cable_segment_against_as, country_or_as)

	overall_stats = [len(item) for item in overall_impacts]

	if not weighted:
		ip_links_weighted_per_country, ip_links_weighted_per_as = {}, {}

	if return_overall_impacts:
		return (overall_stats, overall_impacts, impact_per_country, impact_per_as, ip_links_weighted_per_country, ip_links_weighted_per_as)
	else:
		return (overall_stats, [set(), set(), set(), set(), set(), set()], impact_per_country, impact_per_as, ip_links_weighted_per_country, ip_links_weighted_per_as)


def process_event_ranges (possible_retains, possible_choices, event, cable_segment_against_specifics, total_stats, country_or_as=1, log=None):

	# Loading the relevant data for the given event/disaster
	event_module = importlib.import_module(f'disasters.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	all_arguments = []
	combination_used = []
	count = 0
	for retain in possible_retains:
		for choice in possible_choices:
			all_arguments.append((cable_segment_against_specifics, latlon_to_value, event['search_radius'], event['scope'], {'special': 0, 'retain': retain, 'choice': choice}, count, country_or_as))
			combination_used.append(f'{retain}-{choice}')
			count += 1

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(process_single_event, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		combination = combination_used[index]
		event_stats = result[0]
		stats[combination] = [(es/ts) * 100 for es, ts in zip(event_stats, total_stats)]

	with open(f'results/{event["tag"]}_range_stats', 'wb') as fp:
		pickle.dump(stats, fp)

	print (f'Successfully processed for {event["tag"]}')
	print ('*' * 50)
	print ('\n' * 2)

	return stats


def process_event_global (event, cable_segment_against_specifics, total_stats, specifics_stats_aggregated, specifics_stats_aggregated_data, intra_inter_metadata, country_or_as=1, log=None):

	# Loading the relevant data for the given event/disaster
	event_module = importlib.import_module(f'disasters.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	# There are 3 things we will do with this -- (i) get the overall %age impact for each component, (ii) get the normalized impact at the country/AS level, (iii) get the intra and inter AS component at the country/AS level
	# For (i), we will use the process_single_event function as usual, and using its results, we will get the results for the other 2 

	country_stats_aggregated, as_stats_aggregated, country_stats_aggregated_data, as_stats_aggregated_data = {}, {}, {}, {}

	if country_or_as == 1:
		country_stats_aggregated = specifics_stats_aggregated
		country_stats_aggregated_data = specifics_stats_aggregated_data
	elif country_or_as == 2:
		as_stats_aggregated = specifics_stats_aggregated
		as_stats_aggregated_data = specifics_stats_aggregated_data
	else:
		country_stats_aggregated, as_stats_aggregated = specifics_stats_aggregated
		country_stats_aggregated_data, as_stats_aggregated_data = specifics_stats_aggregated_data

	event_stats, _, impact_per_country, impact_per_as, _, _ = process_single_event (cable_segment_against_specifics, latlon_to_value, event['search_radius'], event['scope'], event['additional_threshold'], country_or_as=country_or_as, individual_impacts=True, return_overall_impacts=False, log=log)

	p1_stats = [(es/ts) * 100 for es, ts in zip(event_stats, total_stats)]

	if country_or_as & 1:
		p2_stats_country = {country: [len(v) / c if c > 0 else 0 for v, c in zip(value, country_stats_aggregated[country])] for country, value in impact_per_country.items()}
		# value[4] has the indices of all AS links and we match these indices to intra (1) or inter (0)
		p3_stats_country_intra = {country: sum([intra_inter_metadata[item] for item in value[4]]) / sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) if sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) > 0 else 0 for country, value in impact_per_country.items()}
		p3_stats_country_inter = {country: (len(value[4]) - sum([intra_inter_metadata[item] for item in value[4]])) / (len(country_stats_aggregated_data[country][4]) - sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]])) if sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) < len(country_stats_aggregated_data[country][4]) else 0 for country, value in impact_per_country.items()}
	
	if country_or_as & 2:
		p2_stats_as = {AS: [len(v) / c if c > 0 else 0 for v, c in zip(value, as_stats_aggregated[AS])] for AS, value in impact_per_as.items()}
		p3_stats_as_intra = {AS: sum([intra_inter_metadata[item] for item in value[4]]) / sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[AS][4]]) if sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[AS][4]]) > 0 else 0 for AS, value in impact_per_as.items()}
		p3_stats_as_inter = {AS: (len(value[4]) - sum([intra_inter_metadata[item] for item in value[4]])) / (len(as_stats_aggregated_data[country][4]) - sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[country][4]])) if sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[country][4]]) < len(as_stats_aggregated_data[country][4]) else 0 for AS, value in impact_per_as.items()}
		
	if country_or_as == 1:
		p2_stats = p2_stats_country
		p3_stats = (p3_stats_country_intra, p3_stats_country_inter)
	elif country_or_as == 2:
		p2_stats = p2_stats_as
		p3_stats = (p3_stats_as_intra, p3_stats_as_inter)
	else:
		p2_stats = (p2_stats_country, p2_stats_as)
		p3_stats = ((p3_stats_country_intra, p3_stats_country_inter), (p3_stats_as_intra, p3_stats_as_inter))

	with open(f'results/{event["tag"]}_global_stats', 'wb') as fp:
		pickle.dump((p1_stats, p2_stats, p3_stats), fp)

	print (f'Successfully processed for {event["tag"]}')
	print ('*' * 50)
	print ('\n' * 2)

	return (p1_stats, p2_stats, p3_stats)


def process_event_baseline_ranges (possible_baselines, event, cable_segment_against_specifics, total_stats, country_or_as=1, log=None):

	# Loading the relevant data for the given event/disaster
	event_module = importlib.import_module(f'disasters.{event["name"]}')	

	all_arguments = []
	count = 0

	for baseline in possible_baselines:
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=baseline)
		all_arguments.append((cable_segment_against_specifics, latlon_to_value, event['search_radius'], event['scope'], event['additional_threshold'], count, country_or_as))
		count += 1

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(process_single_event, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		baseline = possible_baselines[index]
		event_stats = result[0]
		stats[baseline] = [(es/ts) * 100 for es, ts in zip(event_stats, total_stats)]

	with open(f'results/{event["tag"]}_baseline_stats', 'wb') as fp:
		pickle.dump(stats, fp)

	print (f'Successfully processed for {event["tag"]}')
	print ('*' * 50)
	print ('\n' * 2)

	return stats


def process_multiple_events_combined (events, cable_segment_against_specifics, total_stats, specifics_stats_aggregated, specifics_stats_aggregated_data, intra_inter_metadata, country_or_as=1, log=None):

	# This function is very similar to process_events_global as essentially we need the same 3 things to do, but with multiple events combined. So some minor changes to the code from process_events_global

	# Loading the relevant data for the given event/disaster
	latlon_to_values, radii, regions, additional_thresholds = [], [], [], []

	for event in events:
		event_module = importlib.import_module(f'disasters.{event["name"]}')
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

		latlon_to_values.append(latlon_to_value)
		radii.append(event['search_radius'])
		regions.append(event['scope'])
		additional_thresholds.append(event['additional_threshold'])

	country_stats_aggregated, as_stats_aggregated = {}, {}

	if country_or_as == 1:
		country_stats_aggregated = specifics_stats_aggregated
		country_stats_aggregated_data = specifics_stats_aggregated_data
	elif country_or_as == 2:
		as_stats_aggregated = specifics_stats_aggregated
		as_stats_aggregated_data = specifics_stats_aggregated_data
	else:
		country_stats_aggregated, as_stats_aggregated = specifics_stats_aggregated
		country_stats_aggregated_data, as_stats_aggregated_data = specifics_stats_aggregated_data

	event_stats, _, impact_per_country, impact_per_as, _, _ = process_single_event(cable_segment_against_specifics, latlon_to_values, radii, regions, additional_thresholds, country_or_as=country_or_as, individual_impacts=True, return_overall_impacts=False, multiple_events=True, log=log)

	p1_stats = [(es/ts) * 100 for es, ts in zip(event_stats, total_stats)]

	if country_or_as & 1:
		p2_stats_country = {country: [len(v) / c if c > 0 else 0 for v, c in zip(value, country_stats_aggregated[country])] for country, value in impact_per_country.items()}
		p3_stats_country_intra = {country: sum([intra_inter_metadata[item] for item in value[4]]) / sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) if sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) > 0 else 0 for country, value in impact_per_country.items()}
		p3_stats_country_inter = {country: (len(value[4]) - sum([intra_inter_metadata[item] for item in value[4]])) / (len(country_stats_aggregated_data[country][4]) - sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]])) if sum([intra_inter_metadata[item] for item in country_stats_aggregated_data[country][4]]) < len(country_stats_aggregated_data[country][4]) else 0 for country, value in impact_per_country.items()}
	
	if country_or_as & 2:
		p2_stats_as = {AS: [len(v) / c if c > 0 else 0 for v, c in zip(value, as_stats_aggregated[AS])] for AS, value in impact_per_as.items()}
		p3_stats_as_intra = {AS: sum([intra_inter_metadata[item] for item in value[4]]) / sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[AS][4]]) if sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[AS][4]]) > 0 else 0 for AS, value in impact_per_as.items()}
		p3_stats_as_inter = {AS: (len(value[4]) - sum([intra_inter_metadata[item] for item in value[4]])) / (len(as_stats_aggregated_data[country][4]) - sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[country][4]])) if sum([intra_inter_metadata[item] for item in as_stats_aggregated_data[country][4]]) < len(as_stats_aggregated_data[country][4]) else 0 for AS, value in impact_per_as.items()}

	if country_or_as == 1:
		p2_stats = p2_stats_country
		p3_stats = (p3_stats_country_intra, p3_stats_country_inter)
	elif country_or_as == 2:
		p2_stats = p2_stats_as
		p3_stats = (p3_stats_as_intra, p3_stats_as_inter)
	else:
		p2_stats = (p2_stats_country, p2_stats_as)
		p3_stats = ((p3_stats_country_intra, p3_stats_country_inter), (p3_stats_as_intra, p3_stats_as_inter))

	with open(f'results/combined_event_stats', 'wb') as fp:
		pickle.dump((p1_stats, p2_stats, p3_stats), fp)

	print (f'Successfully processed for the combined impact of multiple events')
	print ('*' * 50)
	print ('\n' * 2)

	return p1_stats, p2_stats, p3_stats


def consolidate_stats_at_specific_physical_infra_level (cable_segment_against_specifics, infra_level='country', counts_only=True):

	infra_level_against_specifics = {}	

	if infra_level == 'country':
		landing_points = load_landing_points_dict()
		lp_id_country_dict, _ = get_lp_id_to_country_dict (landing_points)

	for segment, data in cable_segment_against_specifics.items():
		
		if infra_level == 'cable':
			infra_to_do = set([segment[0]])
		else:
			# If infra_level is landing_point leave without entering the nested if statement
			infra_to_do = set([segment[1], segment[2]])
			if infra_level == 'country':
				infra_to_do = set([lp_id_country_dict[lp] for lp in infra_to_do])

		for item in infra_to_do:

			# The default entry has 6 components to store our usual 6 -- cable segments, cables, IP links, IPs, AS links and ASes respectively
			current = infra_level_against_specifics.get(item, [[[] for _ in range(6)] for _ in range(len(data))])

			for index, d in enumerate(data):
				# Checking if there is any data for given country, else skip it
				if len(d[0]) > 0:
					# Adding the cable segment and cable data
					current[index][0].append(segment)
					current[index][1].append(segment[0])
					for dindex, dat in enumerate(d[:4]):
						current[index][2 + dindex].extend(dat)

			infra_level_against_specifics[item] = current			

	infra_level_against_specifics = {item: [[set(part) for part in d] for d in data] for item, data in infra_level_against_specifics.items()}
	infra_level_against_specifics_count = {item: [[len(part) for part in d] for d in data] for item, data in infra_level_against_specifics.items()}

	if counts_only:
		return infra_level_against_specifics_count
	else:
		return infra_level_against_specifics_count, infra_level_against_specifics


def event_independent_analysis (cable_segment_against_specifics, country_stats_aggregated_data, as_stats_aggregated_data, intra_inter_metadata, infra_level='country'):

	# First, let's identify the # of countries/AS cable infrastructure a given country's IP infrastructure is dependent on
	landing_points = load_landing_points_dict()
	lp_id_country_dict, _ = get_lp_id_to_country_dict (landing_points)
	
	depedence_count_country = {}
	for country, data in country_stats_aggregated_data.items():
		involved_segments = data[0]
		involved_cables = data[1]
		involved_landing_points = set(chain(*[item[1:] for item in involved_segments]))
		involved_countries = set([lp_id_country_dict[lp] for lp in involved_landing_points])
		depedence_count_country[country] = [len(item) for item in [involved_segments, involved_cables, involved_landing_points, involved_countries]]

	depedence_count_as = {}
	for AS, data in as_stats_aggregated_data.items():
		involved_segments = data[0]
		involved_cables = data[1]
		involved_landing_points = set(chain(*[item[1:] for item in involved_segments]))
		involved_countries = set([lp_id_country_dict[lp] for lp in involved_landing_points])
		depedence_count_as[AS] = [len(item) for item in [involved_segments, involved_cables, involved_landing_points, involved_countries]]


	# Next, let's examine the %age of intra-AS links used by each country

	intra_as_count_per_country = {country: sum([intra_inter_metadata[item] for item in value[2]]) / len(value[2]) if len(value[2]) > 0 else 0 for country, value in country_stats_aggregated_data.items()}
	intra_as_count_per_as = {AS: sum([intra_inter_metadata[item] for item in value[2]]) / len(value[2]) if len(value[2]) > 0 else 0 for AS, value in as_stats_aggregated_data.items()}

	# Finally, we collect the country vs country stats 

	country_against_country = consolidate_stats_at_specific_physical_infra_level (cable_segment_against_specifics, infra_level=infra_level)

	with open('results/independent_analysis_stats', 'wb') as fp:
		pickle.dump((depedence_count_country, depedence_count_as, intra_as_count_per_country, intra_as_count_per_as, country_against_country), fp)

	print (f'Successfully completed event-independent analysis')
	print ('*' * 50)
	print ('\n' * 2)

	return depedence_count_country, depedence_count_as, intra_as_count_per_country, intra_as_count_per_as, country_against_country


def generate_country_correlation_cluster (country_against_country, all_countries, plot=False, print_res=False):

	df = pd.DataFrame.from_dict(country_against_country, orient='index', columns=all_countries)

	# Computing the correlation matrix and filling NaN with 0
	df = df.corr()
	df = df.fillna(0)
	corr_mat = pd.DataFrame(df)

	# Converting correlation matrix to dissimilarity matrix
	dissimilarity_mat = 1 - corr_mat

	# Doing heirarchical clustering
	linkage = hierarchy.linkage(dissimilarity_mat, method='ward')

	# Creating a dendrogram for visualization (not a final plot we use, but rather to determine thresholds)
	dendrogram = hierarchy.dendrogram(linkage, labels=corr_mat.columns, orientation='top')

	if plot:
		plt.figure(figsize=(12, 8))
		plt.title('Dendogram of Country Correlation Clustering')
		plt.xlabel('Countries')
		plt.ylabel('Distance')
		plt.show()

	max_d = 3
	# clusters = hierarchy.fcluster(linkage, max_d, criterion='distance')
	clusters = hierarchy.fcluster(linkage, 24, criterion='maxclust')

	# This specific part (3 lines) is just to make the colors visibly distinct for countries in different clusters but are close to each other geographically
	mapping = {9: 24, 24: 9, 15: 21, 21: 15}
	vfunc = np.vectorize(lambda x: mapping.get(x, x))
	clusters = vfunc(clusters)

	# Convert cluster assignments to a Pandas DataFrame
	cluster_df = pd.DataFrame({'Variable': corr_mat.columns, 'Cluster': clusters})

	# Print the variables in each cluster
	cluster_id_to_country = {}

	unique_clusters = cluster_df['Cluster'].unique()
	for cluster_id in range(1, len(unique_clusters)+1):
		cluster_elements = cluster_df[cluster_df['Cluster'] == cluster_id]['Variable'].tolist()
		cntry = [pycountry.countries.get(alpha_2=item).name for item in cluster_elements if pycountry.countries.get(alpha_2=item)]
		if print_res:
			print(f'Cluster {cluster_id} Elements: {cntry}')
		cluster_id_to_country[cluster_id] = cntry

	return cluster_df, cluster_id_to_country


def singular_event_impact_analysis (event, cable_segment_against_specifics, total_stats, specifics_stats_aggregated, country_or_as=1, weighted=False, log=None):

	country_stats_aggregated, as_stats_aggregated = {}, {}

	if country_or_as == 1:
		country_stats_aggregated = specifics_stats_aggregated
	elif country_or_as == 2:
		as_stats_aggregated = specifics_stats_aggregated
	else:
		country_stats_aggregated, as_stats_aggregated = specifics_stats_aggregated

	if event['type'] == 'disaster':
		event_module = importlib.import_module(f'disasters.{event["name"]}')
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])
	else:
		latlon_to_value = None

	event_stats, _, impact_per_country, impact_per_as, ip_links_weighted_per_country, ip_links_weighted_per_as = process_single_event(cable_segment_against_specifics, latlon_to_value=latlon_to_value, radius=event['search_radius'], region=event['scope'], additional_threshold=event['additional_threshold'], country_or_as=country_or_as, individual_impacts=True, return_overall_impacts=False, weighted=weighted, log=log)

	p1_stats = [(es/ts) * 100 for es, ts in zip(event_stats, total_stats)]

	if country_or_as & 1:
		# For the index at 2 (ie., IP links stats), we use the ip_links_weighted count value instead (especially for weighted)
		p2_stats_country = {country: [len(v) / c if c > 0 else 0 for v, c in zip(value, country_stats_aggregated[country])] for country, value in impact_per_country.items()}


		p2_stats_country = {}
		for country, value in impact_per_country.items():
			results = []
			for index, (v, c) in enumerate(zip(value, country_stats_aggregated[country])):
				if c != 0:
					if index != 2 or (not weighted):
						results.append(len(v) / c)
					else:
						results.append(ip_links_weighted_per_country.get(country, 0) / c)
				else:
					results.append(0)
			p2_stats_country[country] = results
		
		# We will sort it based on the # of IP links impacted, hence the [3] index below
		p2_stats_country = {country: item for country, item in sorted(p2_stats_country.items(), key=lambda x: x[1][3], reverse=True)}
			
	if country_or_as & 2:

		p2_stats_as = {}
		for AS, value in impact_per_as.items():
			results = []
			for index, (v, c) in enumerate(zip(value, as_stats_aggregated[AS])):
				if c != 0:
					if index != 2 or (not weighted):
						results.append(len(v) / c)
					else:
						results.append(ip_links_weighted_per_as.get(AS, 0) / c)
				else:
					results.append(0)
			p2_stats_as[AS] = results

		p2_stats_as = {AS: item for AS, item in sorted(p2_stats_as.items(), key=lambda x: x[1][3], reverse=True)}

	if country_or_as == 1:
		p2_stats = p2_stats_country
	elif country_or_as == 2:
		p2_stats = p2_stats_as
	else:
		p2_stats = (p2_stats_country, p2_stats_as)
		
	with open(f'results/{event["tag"]}_stats', 'wb') as fp:
		pickle.dump((p1_stats, p2_stats), fp)

	print (f'Successfully processed for {event["tag"]}')
	print ('*' * 50)
	print ('\n' * 2)

	return p1_stats, p2_stats


def process_event_inaccuracies_accounted (event, concise_representations_arguments, total_stats):

	if event['type'] == 'disaster':
		event_module = importlib.import_module(f'disasters.{event["name"]}')
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])
	else:
		latlon_to_value = None

	all_arguments = []
	combination_used = []
	cable_stats_random = []
	count = 0
	for accuracy, representations in concise_representations_arguments.items():
		for representation in representations:
			all_arguments.append((representation, latlon_to_value, event['search_radius'], event['scope'], event['additional_threshold'], count, 1))
			combination_used.append(accuracy)
			cable_segments_count = len(representation)
			cables_count = len(set([item[0] for item in representation.keys()]))
			cable_stats_random.append((cable_segments_count, cables_count))

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(process_single_event, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		total_stats_local = deepcopy(total_stats)
		combination = combination_used[index]
		event_stats = result[0]
		total_stats_local[0] = cable_stats_random[index][0]
		total_stats_local[1] = cable_stats_random[index][1]
		current = stats.get(combination, [])
		current.append([(es/ts) * 100 for es, ts in zip(event_stats, total_stats_local)])
		stats[combination] = current

	stats_final = {combination: [sum(i) / len(i) for i in zip(*stat)] for combination, stat in stats.items()}

	with open(f'results/{event["tag"]}_inaccuracies_accounted_stats', 'wb') as fp:
		pickle.dump(stats_final, fp)

	print (f'Successfully processed for {event["tag"]}')
	print ('*' * 50)
	print ('\n' * 2)

	return stats_final

