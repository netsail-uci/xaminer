import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import pickle, pycountry
import numpy as np
import geopandas as gpd
from colorsys import hls_to_rgb
from matplotlib.colors import LinearSegmentedColormap

import warnings
warnings.filterwarnings('ignore')

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


def generate_prob_dist_impact (range_stats):

	random, top, prob_top = {0: 0}, {0: 0}, {0: 0}
	for key, value in range_stats.items():
		prob = key.split('-')[0]
		ptype = key.split('-')[1]
		if ptype == 'random':
			random[prob] = value
		elif ptype == 'top':
			top[prob] = value
		elif ptype == 'prob_top':
			prob_top[prob] = value 

	return random, top, prob_top


def get_maximal_impact_for_given_events (event_tags, event_top_stats):

	percent_maximal_impact = {}
	types = ['Cable Segment', 'Cable', 'IP links', 'IP', 'AS links', 'AS']

	for index, data in enumerate(event_top_stats):
		res = {types[i]: value for i, value in enumerate(data['1'])}
		percent_maximal_impact[event_tags[index]] = res

	return percent_maximal_impact


def plot_multiple_events_maximal_impact (percent_maximal_impact, event_tags, im_file, bbox_position, legend_cols, ylim=(0, 13)):

	df = pd.DataFrame(percent_maximal_impact).T
	n = len(df.columns)
	custom_palette = sns.color_palette("pastel")
	hatch_patterns = ['o','\\', 'x', '+', '-', '*' ]
	sns.set_style('whitegrid')
	ax = df.plot(kind='bar', figsize=(15, 10), color=custom_palette, edgecolor='black')
	for i, patch in enumerate(ax.patches):
		patch.set_hatch(hatch_patterns[i//len(event_tags)])
	plt.xticks(rotation=0, fontsize=24)
	plt.ylabel('Percentage Impacted', fontsize=24)
	plt.ylim(ylim[0], ylim[1])
	plt.yticks(fontsize=24)
	ax.legend(bbox_to_anchor=bbox_position, loc='upper center', fontsize=24, ncol=legend_cols)
	plt.style.use('ggplot')
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_maximal_impact_for_given_events (event_tags, all_range_stats, im_file, bbox_position=(0.65, 0.95), legend_cols=2, split_text=False, ylim=(0, 13)):

	# Picking the top probability type result
	event_top_stats = [generate_prob_dist_impact(rs)[1] for rs in all_range_stats]
	percent_maximal_impact = get_maximal_impact_for_given_events(event_tags, event_top_stats)

	if split_text:
		event_tags = [f"{' '.join(item.split()[:round(len(item.split())/2)])}\n{' '.join(item.split()[round(len(item.split())/2):])}" for item in event_tags]

	plot_multiple_events_maximal_impact (percent_maximal_impact, event_tags, im_file, bbox_position, legend_cols)


def get_maximal_impact_for_disasters_global (event_tags, all_range_stats):

	percent_maximal_impact = {}
	types = ['Cable Segment', 'Cable', 'IP links', 'IP', 'AS links', 'AS']

	for index, data in enumerate(all_range_stats):
		res = {types[i]: value for i, value in enumerate(data)}
		percent_maximal_impact[event_tags[index]] = res

	return percent_maximal_impact


def plot_maximal_impact_for_disasters_global (event_tags, all_range_stats, im_file, bbox_position=(0.65, 0.95), legend_cols=2, ylim=(0, 80)):

	percent_maximal_impact = get_maximal_impact_for_disasters_global(event_tags, all_range_stats)
	plot_multiple_events_maximal_impact (percent_maximal_impact, event_tags, im_file, bbox_position, legend_cols, ylim)


def dist_plots_all_complete(all_dfs, titles, size, im_file):

	fig, axes = plt.subplots(size, 2, figsize=(size*5, size*5 * (1 + ((size-2)/6))))

	# Define labels for the lines (common for all plots)
	labels = ['Cable Segment', 'Cable', 'IP links', 'IP', 'AS links', 'AS']
	marker_styles = ['o', '^', 's', 'P', '*', 'D']
	sns.set_style('whitegrid')

	for index, d in enumerate(all_dfs):
		row = index // 2
		col = index % 2
		df = pd.DataFrame(d)
		for ind in range(len(df)):
			sns.lineplot(
				x=[float(i) for i in df.columns],
				y=df.iloc[ind],
				label=f'{labels[ind]}',
				legend=False,
				ax=axes[row, col],
				marker=marker_styles[ind], 
				markersize=10 * size / 3, 
				linewidth=2
			)
			axes[row, col].set_title(titles[index], fontsize=20 * size / 3)
			axes[row, col].set_xlabel('Probability', fontsize=20 * size / 3)
			axes[row, col].set_ylabel('Percentage impacted', fontsize=20 * size / 3)
			axes[row, col].tick_params(axis='both', which='both', labelsize=20 * size / 3)

	# Create a common legend for all subplots
	handles, labels = axes[0, 0].get_legend_handles_labels()  # Get handles and labels from the first subplot
	fig.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.08), ncol=len(labels), fontsize=20 * size / 3)

	# Adjust the layout to avoid overlapping titles and labels
	plt.tight_layout()

	plt.subplots_adjust(wspace=0.3, hspace=0.35)
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_probablistic_impact_for_given_events (event_tags, all_range_stats, selection, im_file, size):

	selected_stats, titles = [], []

	for index, event in enumerate(event_tags):
		result = generate_prob_dist_impact(all_range_stats[index])
		for item in selection[event]:
			if item == 'random':
				selected_stats.append(result[0])
				titles.append(f'{event} (Random)')
			elif item == 'top':
				selected_stats.append(result[1])
				titles.append(f'{event} (Top)')
			elif item == 'prob_top':
				selected_stats.append(result[2])
				titles.append(f'{event} (Weighted)')

	dist_plots_all_complete(selected_stats, titles, size, im_file)


def iso2_to_iso3(iso2_code):
	try:
		country = pycountry.countries.get(alpha_2=iso2_code)
		if country:
			return country.alpha_3
	except AttributeError:
		pass 
	return iso2_code


def generate_plots_for_disaster(df, im_file, disaster='Earthquake', special=None, threshold='1 m', size=(15, 10), legend_fontsize=20):
	world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
	world = world.drop(world.loc[world['continent'] == 'Antarctica'].index)

	world = world.merge(df, left_on='iso_a3', right_on='ISO3')

	sns.set(style='whitegrid')
	fig, ax = plt.subplots(1, 1, figsize=size)  # Use the provided size parameter
	world.boundary.plot(ax=ax, linewidth=1, color='black')
	world.plot(column='Value', cmap='Greens', linewidth=0.8, legend=True, ax=ax)

	if special:
		your_country = world[world['name'] == special]
		your_country.boundary.plot(ax=ax, linewidth=3, color='red') 

	# Get the current axes
	cax = plt.gcf().axes[-1]

	# Set the position and size of the legend
	cax.set_position([0.8, 0.3, 0.12, 0.4])  # [x, y, width, height]

	# Set the font size for the legend
	cax.tick_params(axis='both', which='both', labelsize=legend_fontsize)

	plt.xticks(fontsize=20)
	plt.yticks(fontsize=20)
	plt.style.use('ggplot')
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_country_level_impact (country_stats, im_file, disaster='Earthquake', special=None, threshold='1 m', size=(15, 10), legend_fontsize=20):

	# Selecting the IP links fraction impacted (hence the index of 2 is used)
	country_to_norm_sum = {iso2_to_iso3(country): item[2] for country, item in country_stats.items()}
	data = {'ISO3': list(country_to_norm_sum.keys()), 'Value': list(country_to_norm_sum.values())}
	df = pd.DataFrame(data)

	generate_plots_for_disaster(df, im_file, disaster, special, threshold, size, legend_fontsize)


def generate_plots_for_intra_inter_disaster (df, im_file, disaster='Earthquake', column='Intra', special=None):
    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    world = world.drop(world.loc[world['continent'] == 'Antarctica'].index)

    world = world.merge(df, left_on='iso_a3', right_on='ISO3')

    sns.set(style='whitegrid')
    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    world.boundary.plot(ax=ax, linewidth=1, color='black')
    world.plot(column=column, cmap='Greens', linewidth=0.8, legend=True, ax=ax)

    if special:
        your_country = world[world['name'] == special]
        your_country.boundary.plot(ax=ax, linewidth=3, color='red') 

    # Get the current axes
    cax = plt.gcf().axes[-1]

    # Set the position and size of the legend
    cax.set_position([0.8, 0.3, 0.1, 0.4])  # [x, y, width, height]
    
    cax.tick_params(axis='both', which='both', labelsize=20)

    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.style.use('ggplot')
    plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
    plt.show()


def plot_country_level_intra_inter_distribution (country_stats, im_file, disaster='Earthquake', column='Intra', special=None):

	intra_stats, inter_stats = country_stats
	country_intra_impacted = {iso2_to_iso3(country): value for country, value in intra_stats.items()}
	country_inter_impacted = {iso2_to_iso3(country): value for country, value in inter_stats.items()}
	data = {'ISO3': list(country_intra_impacted.keys()), 'Intra': list(country_intra_impacted.values()), 'Inter': list(country_inter_impacted.values())}
	df = pd.DataFrame(data)

	im_file += f'_{column.lower()}_plot.pdf'

	generate_plots_for_intra_inter_disaster (df, im_file, disaster, column, special)


def generate_range_plot (df, im_file, disaster='Sea rise (in meters)'):
    
	sns.set_style("whitegrid")
	plt.figure(figsize=(15, 10)) 
	labels = ['Cable Segment', 'Cable', 'IP links', 'IP', 'AS links', 'AS']
	line_styles = ['-', '--', '-.', ':', 'solid', 'dashed']
	marker_styles = ['o', '^', 's', 'P', '*', 'D']
	for index in range(len(df)):
		sns.lineplot(x=[float(i) for i in df.columns], y=df.iloc[index], label=f'{labels[index]}', marker=marker_styles[index], markersize=10, linewidth=5, linestyle=line_styles[index])

	plt.xlabel(f'{disaster}', fontsize=28)
	plt.ylabel('Percentage impacted', fontsize=28)
	plt.xticks(fontsize=28)
	plt.yticks(fontsize=28)
	plt.legend(fontsize=28)
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_disaster_baseline_impacts (disaster_stats, im_file, disaster='Sea rise (in meters)'):

	df = pd.DataFrame(disaster_stats)
	generate_range_plot (df, im_file, disaster)


def plot_stats_multiple_disasters_merged (disaster_stats, im_file):

	n = 6
	custom_palette = sns.color_palette("Set1")
	hatch_patterns = ['\\', 'x', '+', '|', '*', 'o']
	bar_positions = np.arange(len(disaster_stats))
	sns.set_style('whitegrid')

	fig, ax = plt.subplots(figsize=(15, 10))

	bars = ax.bar(bar_positions, disaster_stats, color=custom_palette, edgecolor='black', width=0.25)
	ax.set_xticks([])
	plt.xticks(bar_positions, labels = ['Cable\nSegment', 'Cable', 'IP\nLink', 'IP', 'AS\nLink', 'AS'], rotation=0, fontsize=30)
	plt.ylabel('Percentage Impacted', fontsize=30)
	plt.ylim(0, 40)
	plt.yticks(fontsize=40)

	plt.style.use('ggplot')
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_country_dependence (df, im_file, unwanted_continents=None, unwanted_countries=None, figsize=(10, 20), xlim=None, ylim=None, cax_position=[0.8, 0.44, 0.1, 0.12], special=None):

	# Load world map data
	world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
	world = world.drop(world.loc[world['continent'] == 'Antarctica'].index)

	if unwanted_continents or unwanted_countries:
		unwanted_continents = ['Africa', 'Asia', 'North America', 'South America', 'Oceania']
		unwanted_countries = ['Russia', 'Fr. S. Antarctic Lands']
		world = world[~world['continent'].isin(unwanted_continents)]
		world = world[~world['name'].isin(unwanted_countries)]

	# Merge your DataFrame with world map data
	world = world.merge(df, left_on='iso_a3', right_on='ISO3')

	# Set Seaborn style
	sns.set(style='whitegrid')

	fig, ax = plt.subplots(1, 1, figsize=figsize)

	# Set latitude and longitude limits
	if xlim:
		ax.set_xlim(xlim) 
	if ylim: 
		ax.set_ylim(ylim)  

	# Plot map boundaries
	if special:
		world.boundary.plot(ax=ax, linewidth=1, color='black')
	else:
		world.boundary.plot(ax=ax, linewidth=0.5, color='black')

	# Plot countries with colors based on the 'Value' column
	world.plot(column='Value', cmap='Greens', linewidth=0.8, legend=True, ax=ax)

	if special:
		your_country = world[world['name'] == special]
		your_country.boundary.plot(ax=ax, linewidth=5, color='red') 

	# Get the current axes
	cax = plt.gcf().axes[-1]

	# Set the position and size of the legend
	cax.set_position(cax_position)  # [x, y, width, height]

	plt.style.use('ggplot')

	if special:
		cax.tick_params(axis='both', which='both', labelsize=18) 
		plt.xticks(fontsize=18)
		plt.yticks(fontsize=18)
	
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_dependence_count_by_country (stats, im_file, unwanted_continents=None, unwanted_countries=None, figsize=(10, 20), xlim=None, ylim=None, cax_position=[0.8, 0.44, 0.1, 0.12], special=None):

	# We are interested in the number of countries (hence the index of 3)
	country_to_len = {iso2_to_iso3(country): value[3] for country, value in stats.items()}
	data = {'ISO3': list(country_to_len.keys()), 'Value': list(country_to_len.values())}
	df = pd.DataFrame(data)

	plot_country_dependence(df, im_file, unwanted_continents, unwanted_countries, figsize, xlim, ylim, cax_position, special)


def ecdf(data):
    x = np.sort(data)
    y = np.arange(1, len(data) + 1) / len(data)
    return x, y


def get_ecdf_plots (lengths, im_file, xlim=None):

	sns.set_style('whitegrid')
	plt.figure(figsize=(12, 8))

	linestyles = ['-', '--', '-.', ':']

	for index, (key, value) in enumerate(lengths.items()):
		x, y = ecdf(value)
		plt.plot(x, y, label=key, linestyle=linestyles[index], linewidth=3)
	
	# Add labels and legend
	plt.xlabel('Physical Infrastructure Components', fontsize=24)
	plt.ylabel('CDF', fontsize=24)
	plt.xticks(fontsize=24)
	plt.yticks(fontsize=24)
	plt.legend(title='', labels=lengths.keys())
	plt.legend(fontsize=24)
	if xlim:
		plt.xlim(0,xlim)

	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_ecdf_plots (dependence_stats, im_file, xlim=None):

	phy_infra = ['Cable Segment', 'Cable', 'Landing Point', 'Country']

	lengths = {}

	for ls in dependence_stats.values():
		for infra, length in zip(phy_infra, ls):
			current = lengths.get(infra, [])
			current.append(length)
			lengths[infra] = current

	get_ecdf_plots (lengths, im_file, xlim=xlim)


def get_distinct_colors(n, l_min=40, l_max=70, s_min=60, s_max=100):

	colors = []

	for i in np.arange(0., 360., 360. / n):
		h = i / 360.
		l = (l_min + np.random.rand() * (l_max - l_min)) / 100.
		s = (s_min + np.random.rand() * (s_max - s_min)) / 100.
		colors.append(hls_to_rgb(h, l, s))

	return colors


def generate_correlation_plot (cluster_df, im_file, cluster_len):

	cluster_df['Country'] = cluster_df['Variable'].apply(iso2_to_iso3)

	colors = get_distinct_colors(cluster_len)
	# Some modifications to make the colors more visibly distinct
	colors[-1] = '#808080'
	colors[3] = '#FFFF00'
	sns.set_style('whitegrid')

	custom_colormap = LinearSegmentedColormap.from_list('custom', colors)

	world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
	world = world.drop(world.loc[world['continent'] == 'Antarctica'].index)

	merged = world.set_index('iso_a3').join(cluster_df.set_index('Country'))
	fig, ax = plt.subplots(1, 1, figsize=(15, 10))
	world.boundary.plot(ax=ax, linewidth=1, color='black')
	merged.plot(column='Cluster', cmap=custom_colormap, linewidth=0.8, ax=ax, legend=False)

	plt.xticks(fontsize=20)
	plt.yticks(fontsize=20)
	plt.style.use('ggplot')
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()


def plot_country_level_percent_intra_distribution (country_stats, im_file, special=None):

	country_percent_intra = {iso2_to_iso3(country): value for country, value in country_stats.items()}
	data = {'ISO3': list(country_percent_intra.keys()), 'Values': list(country_percent_intra.values())}
	df = pd.DataFrame(data)

	generate_plots_for_intra_inter_disaster (df, im_file, '', 'Values', special)


def get_maximal_impact_accounting_inaccuracies (result):

	percent_impact_maximal = {}
	types = ['Cable Segment', 'Cable', 'IP links', 'IP', 'AS links', 'AS']
	for accuracy, data in result.items():
		res = {types[i]: value for i, value in enumerate(data)}
		percent_impact_maximal[tuple([item * 100 for item in accuracy])] = res

	return percent_impact_maximal


def plot_inaccuracies_accounted_maximal_impact (stats, im_file, accuracies_list_size):

	percent_impact_maximal = get_maximal_impact_accounting_inaccuracies (stats)
	
	df = pd.DataFrame(percent_impact_maximal).T
	n = len(df.columns)
	
	custom_palette = sns.color_palette("pastel")
	hatch_patterns = ['o','\\', 'x', '+', '-', '*' ]
	
	sns.set_style('whitegrid')
	ax = df.plot(kind='bar', figsize=(15, 10), color=custom_palette, edgecolor='black')
	for i, patch in enumerate(ax.patches):
		patch.set_hatch(hatch_patterns[i//accuracies_list_size])
	
	plt.xticks(rotation=0, fontsize=20)
	plt.ylabel('Percentage Impacted', fontsize=20)
	plt.xlabel('(Correct (top), Correct (non-top), Incorrect) (predictions in percentage)', fontsize=20)
	plt.ylim(0, 16)
	plt.yticks(fontsize=20)
	ax.legend(bbox_to_anchor=(0.5, 0.95), loc='upper center', fontsize=20, ncol=3)
	plt.style.use('ggplot')
	plt.savefig(im_file, bbox_inches='tight', pad_inches=0.3)
	plt.show()
