# Xaminer

This repository contains the associated codebase for "Xaminer: An Internet Cross-Layer Resilience Analysis Tool". Please cite as : "Alagappan Ramanathan, Rishika Sankaran, and Sangeetha Abdu Jyothi. 2024. Xaminer: An Internet Cross-Layer Resilience Analysis Tool. Proc. ACM Meas. Anal. Comput. Syst. 8, 1, Article 16 (March 2024), 37 pages. https://doi.org/10.1145/3639042"

This repository contains "paper-version-code" which includes the code and plots from the paper. The scripts associated with the most recent version of the Xaminer tool can be found in the scripts directory. A detailed documentation for using the tool is detailed in the [README](https://gitlab.com/netsail-uci/xaminer/-/blob/main/scripts/README.md) file within the scripts directory. 

The required public libraries to run the tool can be found in the "requirements.txt" file. Since this codebase depends on certain data sources (which are large files), this repository doesn't include these large files and would throw an error, while suggesting locations from where the relevant files can be downloaded and where these files need to be placed. Once all the requirements are satisfied, to run the codebase, use "python scripts/xaminer.py" from the root directory to avoid library import issues.

NOTE: Currently all the analysis experiments in the "scripts/xaminer.py" have been set to not run by default. To run a specific experiment, you need to set the flag relevant to that experiment in the "scripts/xaminer.py" file. Please refer to the detailed documentation (README file in the scripts directory) for more information on how to do so.

In case of any issues with the codebase, reach out to Alagappan Ramanathan (alagappr@uci.edu).
