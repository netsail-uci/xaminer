from utils.nautilus_data_utils import *
from utils.analysis_utils import *
from utils.plot_utils import *
import importlib, multiprocessing
import pickle
from copy import deepcopy

def global_process_func (event, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, metadata, log=False, print_top=10, print_res=False):

	event_module = importlib.import_module(f'disaster_process.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	filt_segments, filt_cables, filt_ip_links, filt_ips, filt_as_links, filt_ases = get_coverage_stats_given_scope (cable_segment_to_all_info_map, latlon_to_value, radius=event['search_radius'], region=event['scope'], additional_thresholds=event['additional_threshold'], log=log)

	event_matrix_info = ranker_function(countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, filt_segments, print_top=print_top, print_res=print_res)
	
	event_filt_info = {
						'Cable Segment': filt_segments,
						'Cable': filt_cables,
						'IP links': filt_ip_links,
						'IP': filt_ips,
						'AS links': filt_as_links,
						'AS': filt_ases
				 	}

	event_info = {'Metadata': metadata, 'Filter Info': event_filt_info, 'Matrix info': event_matrix_info}

	with open(f'results/{event["name"]}_global_details', 'wb') as fp:
		pickle.dump(event_info, fp)

	print (f'Successfully processed for {event["name"]}')

	return filt_segments


def global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata):

	event_module = importlib.import_module(f'disaster_process.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	all_arguments = []
	combination_used = []
	count = 0
	for retain in possible_retains:
		for choice in possible_choices:
			all_arguments.append((cable_segment_to_all_info_map, latlon_to_value, event['search_radius'], event['scope'], {'special': 0, 'retain': retain, 'choice': choice}, count))
			combination_used.append(f'{retain}-{choice}')
			count += 1

	print (f'We will run the calculation function for {len(all_arguments)} combinations')

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(get_coverage_stats_given_scope, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		combination = combination_used[index]
		current = stats.get(combination, [[], [], [], [], [], []])
		[item.append(len(result[index])) for index, item in enumerate(current)]
		stats[combination] = current

	unified_stats = {}
	for combination, stat in stats.items():
		unified_stats[combination] = [sum(item)/len(item) for item in stat]

	result = {'Metadata': metadata, 'Stats': unified_stats}

	with open(f'results/{event["tag"]}_range_stats', 'wb') as fp:
		pickle.dump(result, fp)

	print (f'Successfully processed for {event["name"]}')
	print ('*' * 50)
	print ('\n' * 2)

def global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata):

	event_module = importlib.import_module(f'disaster_process.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	# We will run each experiment 10 times to get normalized results

	all_arguments = []
	combination_used = []
	count = 0
	for accuracy in accuracies_list:
		for i in range(10):
			cable_segment_to_all_info_map_random = generate_cable_segments_to_all_info_map_random (conditioned_mapping, ip_to_asn_map, ip_to_best_geolocation)
			cable_segments_count = len(set(list(cable_segment_to_all_info_map.keys()) + list(cable_segment_to_all_info_map_random.keys())))
			# cables_count = len(set([item[0] for item in cable_segment_to_all_info_map] + [item[0] for item in cable_segment_to_all_info_map_random]))
			# print (f'Length of cable segments random map : {len(cable_segment_to_all_info_map_random)}')
			all_arguments.append((cable_segment_to_all_info_map, cable_segment_to_all_info_map_random, accuracy, latlon_to_value, event['search_radius'], event['scope'], {'special': 0}, count))
			combination_used.append((f'{accuracy[0]}-{accuracy[1]}-{accuracy[2]}', cable_segments_count))
			count += 1
		print (f'Generated all arguments for {accuracy}')

	print (f'We will run the calculation function for {len(all_arguments)} combinations')

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(get_coverage_stats_given_scope_inaccuracy_accounted, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		combination = combination_used[index][0]
		cable_segments_count = combination_used[index][1]
		# cables_count = combination_used[index][2]
		current = stats.get(combination, [[], [], [], [], [], []])
		for index, item in enumerate(current):
			if index == 0:
				item.append(len(result[index]) / cable_segments_count)
			else:
				item.append(len(result[index]))
		stats[combination] = current

	unified_stats = {}
	for combination, stat in stats.items():
		unified_stats[combination] = [sum(item)/len(item) for item in stat if len(item) > 0]

	result = {'Metadata': metadata, 'Stats': unified_stats}

	with open(f'results/{event["tag"]}_inaccuracies_accounted_stats', 'wb') as fp:
		pickle.dump(result, fp)

	print (f'Successfully processed for {event["name"]}')
	print ('*' * 50)
	print ('\n' * 2)


def global_process_level_ranges_func (possible_baselines, cable_segment_to_all_info_map, event, metadata):

	event_module = importlib.import_module(f'disaster_process.{event["name"]}')
	
	all_arguments = []
	combination_used = []
	count = 0
	for baseline in possible_baselines:
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=baseline)
		all_arguments.append((cable_segment_to_all_info_map, latlon_to_value, event['search_radius'], event['scope'], {'special': 0}, count))
		combination_used.append(baseline)
		count += 1

	print (f'We will run the calculation function for {len(all_arguments)} combinations')

	p = multiprocessing.Pool(processes=25)
	results = p.starmap(get_coverage_stats_given_scope, all_arguments)
	p.close()
	p.join()

	stats = {}
	for index, result in enumerate(results):
		combination = combination_used[index]
		current = stats.get(combination, [[], [], [], [], [], []])
		[item.append(len(result[index])) for index, item in enumerate(current)]
		stats[combination] = current

	unified_stats = {}
	for combination, stat in stats.items():
		unified_stats[combination] = [sum(item)/len(item) for item in stat]

	result = {'Metadata': metadata, 'Stats': unified_stats}

	with open(f'results/{event["name"]}_range_stats', 'wb') as fp:
		pickle.dump(result, fp)

	print (f'Successfully processed for {event["name"]}')
	print ('*' * 50)
	print ('\n' * 2)


def get_and_merge_disaster_results (event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False):

	event_module = importlib.import_module(f'disaster_process.{event["name"]}')
	latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])

	filt_segments, filt_cables, filt_ip_links, filt_ips, filt_as_links, filt_ases = get_coverage_stats_given_scope (cable_segment_to_all_info_map, latlon_to_value, radius=event['search_radius'], region=event['scope'], additional_thresholds=event['additional_threshold'], log=log)

	filt_network_country_to_intra_inter_as_links = generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map, filt_segments)

	event_filt_info = {
						'Cable Segment': filt_segments,
						'Cable': filt_cables,
						'IP links': filt_ip_links,
						'IP': filt_ips,
						'AS links': filt_as_links,
						'AS': filt_ases
				 	}

	total_impacted_segments.update(filt_segments)
	total_impacted_cables.update(filt_cables)
	total_impacted_ip_links.update(filt_ip_links)
	total_impacted_ips.update(filt_ips)
	total_impacted_as_links.update(filt_as_links)
	total_impacted_ases.update(filt_ases)

	event_matrix_info = ranker_function(countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, filt_segments, print_top=print_top, print_res=print_res)

	print (f'Successfully processed {event["name"]}')

	return event_matrix_info, event_filt_info, filt_network_country_to_intra_inter_as_links


def individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False):

	if event['type'] == 'disaster':
		event_module = importlib.import_module(f'disaster_process.{event["name"]}')
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])
	else:
		latlon_to_value = None

	filt_segments, filt_cables, filt_ip_links, filt_ips, filt_as_links, filt_ases = get_coverage_stats_given_scope(cable_segment_to_all_info_map, latlon_to_value, radius=event['search_radius'], region=event['scope'], additional_thresholds=event['additional_threshold'], log=False)

	event_matrix_info = ranker_function(countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, filt_segments, print_top=print_top, print_res=print_res)

	event_filt_info = {
						'Cable Segment': filt_segments,
						'Cable': filt_cables,
						'IP links': filt_ip_links,
						'IP': filt_ips,
						'AS links': filt_as_links,
						'AS': filt_ases
				 	}

	event_info = {'Metadata': metadata, 'Filter Info': event_filt_info, 'Matrix info': event_matrix_info}

	with open(f'results/{event["tag"]}_details', 'wb') as fp:
		pickle.dump(event_info, fp)

	print (f'Finished processing for {event["tag"]}')


def individual_event_process_func_v2 (event, cable_segment_to_all_info_map, ip_to_country):

	if event['type'] == 'disaster':
		event_module = importlib.import_module(f'disaster_process.{event["name"]}')
		latlon_to_value, _ = event_module.load_dataset(baseline_threshold=event['baseline'])
	else:
		latlon_to_value = None

	filt_segments, filt_cables, filt_ip_links, filt_ips, filt_as_links, filt_ases = get_coverage_stats_given_scope(cable_segment_to_all_info_map, latlon_to_value, radius=event['search_radius'], region=event['scope'], additional_thresholds=event['additional_threshold'], log=False)

	country_to_count_filt = generate_count_by_country(list(filt_ip_links), ip_to_country)

	with open(f'results/{event["tag"]}_details', 'wb') as fp:
		pickle.dump(country_to_count_filt, fp)

	print (f'Finished processing for {event["tag"]}')



if __name__ == '__main__':

	# See detailed comments below for experiments related to each (sub)section to find out how to run for new data source
	# Note: Internally most of the main functions that generate the result inherently call "get_coverage_stats_given_scope" function from utils/analysis_utils.py and structures the results to match the requirements for each experiment

	# Section 5.1 and 5.3.1 (includes Appendix B.1 and B.3) results data (to enable/replicate set the below flag to True)
	disaster_combinations = False 

	if disaster_combinations:

		# This includes only definite submarine IPv4 links
		event = { 
				'category_condition': 'oc', 
				'ip_version': 4
				}

		# Loading the required data from Nautilus (3 lines)
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Getting concise representation and data
		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		metadata = {
					'Cable Segments': list(cable_segment_to_all_info_map.keys()),
					'Cable': all_cables,
					'IP links': all_ip_links,
					'IP': all_ips,
					'AS links': all_as_links,
					'AS': all_ases
					}

		# Examining various probabilities of failures and sampling distribution (given by possible_retains and possible_choices respectively)
		# For sampling distributions that involve randomness, results are averaged over 10 runs
		possible_retains = [0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
		possible_choices = ['random'] * 10 + ['top'] + ['prob_top'] * 10
		possible_choices_searise = ['random'] * 10 + ['top'] + ['prob_top_rev'] * 10

		# First all the earthquakes
		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['tw', 'ph']},
						 'tag': 'TW-PH-earthquake'
						 }
		event.update(event_updates)

		# This function runs all combinations of failure probabilities and sampling distributions
		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['jp']},
						 'tag': 'Japan-earthquake'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'regions': ['Washington', 'Oregon', 'British Columbia']},
						 'tag': 'PNW-earthquake'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['id']},
						 'tag': 'Indonesia-earthquake'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		# Next all the hurricanes

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['cu', 'jm', 'ht', 'do', 'bs']},
						 'tag': 'Carribbean-Hurricane'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['gb', 'ie']},
						 'tag': 'British-Isles-Hurricane'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['bz', 'hn', 'ni', 'pa', 'cr']},
						 'tag': 'Central-America-Hurricane'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'regions': ['Florida', 'Louisiana', 'Texas']},
						 'tag': 'Florida-Hurricane'
						 }
		event.update(event_updates)

		global_process_ranges_func (possible_retains, possible_choices, cable_segment_to_all_info_map, event, metadata)	


	# Section 5.2.1 and 5.2.2 (includes Appendix B.2) results data (to enable/replicate set the below flag to True)
	disaster_global = False 

	if disaster_global:

		# The following example uses "definite submarine" IPv4 links from Nautilus
		event = {
				'type': 'disaster', 
				'category_condition': 'oc', 
				'ip_version': 4,
				'scope': None,
				'additional_threshold': {'special': 0}
				}

		# Generating the baseline data

		# In the following 3 lines, we load IP to cable mapping, IP to ASN mapping and IP geolocation data from Nautilus
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Generate the intermediate concise representations 
		
		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count = generate_cable_segment_to_country_as_maps (cable_segment_to_all_info_map, all_ases)

		cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count)

		network_country_to_intra_inter_as_count = generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map)

		metadata = {
					'Cable Segments': list(cable_segment_to_all_info_map.keys()),
					'Cable': all_cables,
					'IP links': all_ip_links,
					'IP': all_ips,
					'AS links': all_as_links,
					'AS': all_ases
					}

		total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases = set(), set(), set(), set(), set(), set()

		# Now let's look at each disaster specifically
		# Using the name, Xaminer attempts to load the relevant disaster data directly (ie., if earthquake if used, load_dataset function from "disaster_process/earthquake.py" is run)
		# Baseline parameter represents the impact threshold
		# search_radius represents the spatial impact radius 
		event_updates = {
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10
						 }
		event.update(event_updates)

		# The following line processes this and generates the stats for Earthquake
		earthquake_matrix_info, earthquake_filt_info, earthquake_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		# Similarly done for other disasters
		event_updates = {
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50
						 }
		event.update(event_updates)

		hurricane_matrix_info, hurricane_filt_info, hurricane_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		event_updates = {
						 'name': 'searise',
						 'baseline': 1,
						 'search_radius': 10
						}
		event.update(event_updates)

		searise_matrix_info, searise_filt_info, searise_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		event_updates = {
						 'name': 'solarstorm',
						 'baseline': 50,
						 'search_radius': 1
						}
		event.update(event_updates)

		solarstorm_matrix_info, solarstorm_filt_info, solarstorm_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		# The total_* parameter in each of the earlier function aggregates the data across all disasters. Use the next 3 lines if you need to combined profile for all disasters (Note: The paper doesn't have plots related to maximum infrastructure risk for all disasters combined)
		total_impacted_as_links = merge_ab_ba_as_links(total_impacted_as_links)

		total_network_country_to_intra_inter_as_count = generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map, total_impacted_segments)

		event_matrix_info = ranker_function(countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, total_impacted_segments, print_top=10, print_res=False)
	
		# The below dictionaries combines all the data into a single structure. This can be edited as needed, but to run with the plot scripts follow a similar pattern or modify plot scripts
		event_filt_info = {
							'Cable Segment': total_impacted_segments,
							'Cable': total_impacted_cables,
							'IP links': total_impacted_ip_links,
							'IP': total_impacted_ips,
							'AS links': total_impacted_as_links,
							'AS': total_impacted_ases
					 	}

		ind_info = {
						   'Earthquake': {'Matrix info' : earthquake_matrix_info, 'Filt Info': earthquake_filt_info},
						   'Hurricane': {'Matrix info' : hurricane_matrix_info, 'Filt Info': hurricane_filt_info},
						   'Searise': {'Matrix info' : searise_matrix_info, 'Filt Info': searise_filt_info},
						   'Solarstorm': {'Matrix info' : solarstorm_matrix_info, 'Filt Info': solarstorm_filt_info},
						   }

		network_country_ii_info = {
							'Metadata': network_country_to_intra_inter_as_count,
							'Earthquake': earthquake_network_country_to_intra_inter_as_count,
							'Hurricane': hurricane_network_country_to_intra_inter_as_count,
							'Searise': searise_network_country_to_intra_inter_as_count,
							'Solarstorm': solarstorm_network_country_to_intra_inter_as_count,
							'All': total_network_country_to_intra_inter_as_count
		}

		event_info = {'Metadata': metadata, 'Filter Info': event_filt_info, 'Matrix info': event_matrix_info, 'Individual info': ind_info, 'Network Country II info': network_country_ii_info}

		# Storing the data. Will be loaded later in the plot scripts
		with open('results/merged_global_details_maximum_impact', 'wb') as fp:
			pickle.dump(event_info, fp)

		print ('Successfully generated the required data')


	# Section 5.2.3 results data (to enable/replicate set the below flag to True)
	disaster_ranges = False 

	if disaster_ranges:

		event = {
				'type': 'disaster', 
				'category_condition': 'oc', 
				'ip_version': 4,
				'scope': None,
				'additional_threshold': {'special': 0}
				}

		# Loading data from Nautilus in the following 3 lines
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Generating the overall stats at each layer in the following 3 lines and storing it as metadata
		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		metadata = {
					'Cable Segments': list(cable_segment_to_all_info_map.keys()),
					'Cable': all_cables,
					'IP links': all_ip_links,
					'IP': all_ips,
					'AS links': all_as_links,
					'AS': all_ases
					}

		# In the below 4 lines, we examine different ranges (impact threshold) of sea level rise with a search radius (spatial probing radius) of 10 km 
		possible_baselines = [0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2]

		# Now processing Searise
		event_updates = {
						 'name': 'searise',
						 'search_radius': 10
						}
		event.update(event_updates)

		# This function generates the results for each combination (Note: This uses multiprocessing Pool. Modify the code if this needs to be disabled)
		global_process_level_ranges_func (possible_baselines, cable_segment_to_all_info_map, event, metadata)

		# Similarly done for solarstorms with various impact thresholds
		possible_baselines = [70, 65, 60, 55, 50, 45, 40]

		event_updates = {
						 'name': 'solarstorm',
						 'search_radius': 1
						}
		event.update(event_updates)

		global_process_level_ranges_func (possible_baselines, cable_segment_to_all_info_map, event, metadata)

	
	# Section 5.3.2 results data (to enable/replicate set the below flag to True)
	multiple_disasters = False 

	if multiple_disasters:
		# The following example uses "definite submarine" IPv4 links from Nautilus
		event = {
				'type': 'disaster', 
				'category_condition': 'oc', 
				'ip_version': 4,
				'scope': None,
				}

		# In the following 3 lines, we load IP to cable mapping, IP to ASN mapping and IP geolocation data from Nautilus
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Generate the intermediate concise representations 

		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count = generate_cable_segment_to_country_as_maps (cable_segment_to_all_info_map, all_ases)

		cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count)

		network_country_to_intra_inter_as_count = generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map)

		metadata = {
					'Cable Segments': list(cable_segment_to_all_info_map.keys()),
					'Cable': all_cables,
					'IP links': all_ip_links,
					'IP': all_ips,
					'AS links': all_as_links,
					'AS': all_ases
					}

		total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases = set(), set(), set(), set(), set(), set()

		# Let's do each disaster now
		# For each disaster, we consider the scenario of top 5% to be impacted (except searise, where we use impact threshold of 0.3 m increase in sea rise, as unlike other disasters, if searise happens, there is 100% probability of network equipment failure)
		event_updates = {
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
						 }
		event.update(event_updates)

		earthquake_matrix_info, earthquake_filt_info, earthquake_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		event_updates = {
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
						 }
		event.update(event_updates)

		hurricane_matrix_info, hurricane_filt_info, hurricane_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		event_updates = {
						 'name': 'searise',
						 'baseline': 0.3,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0}
						}
		event.update(event_updates)

		searise_matrix_info, searise_filt_info, searise_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		event_updates = {
						 'name': 'solarstorm',
						 'baseline': 50,
						 'search_radius': 1,
						 'additional_threshold': {'special': 0, 'retain': 0.05, 'choice': 'top'}
						}
		event.update(event_updates)

		solarstorm_matrix_info, solarstorm_filt_info, solarstorm_network_country_to_intra_inter_as_count = get_and_merge_disaster_results(event, cable_segment_to_all_info_map, total_impacted_segments, total_impacted_cables, total_impacted_ip_links, total_impacted_ips, total_impacted_as_links, total_impacted_ases, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False, log=False)

		# The total_* parameter in each of the earlier function aggregates the data across all disasters. Use the next 3 lines if you need to combined profile for all disasters (Note: The paper doesn't have plots related to maximum infrastructure risk for all disasters combined)
		total_impacted_as_links = merge_ab_ba_as_links(total_impacted_as_links)

		total_network_country_to_intra_inter_as_count = generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map, total_impacted_segments)

		event_matrix_info = ranker_function(countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, total_impacted_segments, print_top=10, print_res=False)
	
		event_filt_info = {
							'Cable Segment': total_impacted_segments,
							'Cable': total_impacted_cables,
							'IP links': total_impacted_ip_links,
							'IP': total_impacted_ips,
							'AS links': total_impacted_as_links,
							'AS': total_impacted_ases
					 	}

		ind_matrix_info = {
						   'Earthquake': {'Matrix info' : earthquake_matrix_info, 'Filt Info': earthquake_filt_info},
						   'Hurricane': {'Matrix info' : hurricane_matrix_info, 'Filt Info': hurricane_filt_info},
						   'Searise': {'Matrix info' : searise_matrix_info, 'Filt Info': searise_filt_info},
						   'Solarstorm': {'Matrix info' : solarstorm_matrix_info, 'Filt Info': solarstorm_filt_info},
						   }

		network_country_ii_info = {
							'Metadata': network_country_to_intra_inter_as_count,
							'Earthquake': earthquake_network_country_to_intra_inter_as_count,
							'Hurricane': hurricane_network_country_to_intra_inter_as_count,
							'Searise': searise_network_country_to_intra_inter_as_count,
							'Solarstorm': solarstorm_network_country_to_intra_inter_as_count,
							'All': total_network_country_to_intra_inter_as_count
		}

		event_info = {'Metadata': metadata, 'Filter Info': event_filt_info, 'Matrix info': event_matrix_info, 'Individual Matrix info': ind_matrix_info, 'Network Country II info': network_country_ii_info}

		with open('results/merged_global_details', 'wb') as fp:
			pickle.dump(event_info, fp)

		print ('Successfully generated the required data')


	# Section 5.4 results data (to enable/replicate set the below flag to True)
	independent_analysis = False 

	if independent_analysis:
		# The following example uses "definite submarine" IPv4 links from Nautilus
		event = { 
				'category_condition': 'oc', 
				'ip_version': 4
				}

		# In the following 3 lines, we load IP to cable mapping, IP to ASN mapping and IP geolocation data from Nautilus
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Constructing the concise representation in the below lines
		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map_v2 (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count = generate_cable_segment_to_country_as_maps_with_data (cable_segment_to_all_info_map, all_ases)

		cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count)

		all_data = {'Cable Segment': {'Country': cable_segment_against_country, 'AS': cable_segment_against_ases, 'Intra Inter': cable_segment_intra_inter_as_count},
					'Cable': {'Country': cable_against_country, 'AS': cable_against_ases, 'Intra Inter': cable_intra_inter_as_count},
					'Landing Point': {'Country': landing_point_against_country, 'AS': landing_point_against_ases, 'Intra Inter': landing_point_intra_inter_as_count},
					'Country': {'Country': country_against_country, 'AS': country_against_ases, 'Intra Inter': country_intra_inter_as_count},
					'Metadata': {'Country': countries, 'AS': ases}}

		with open('results/independent_analysis_all_data_v3', 'wb') as fp:
			pickle.dump(all_data, fp)

		all_data_corr = {}

		for phy_location, item in all_data.items():
			if phy_location != 'Metadata':
				for con_or_as, data in item.items():
					current = all_data_corr.get(phy_location, {})
					current[con_or_as] = generate_correlation_dataframe(data, all_data['Metadata'][con_or_as])
					all_data_corr[phy_location] = current

		with open('results/independent_analysis_all_data_corr', 'wb') as fp:
			pickle.dump(all_data_corr, fp)


	# Section 5.5 results data (to enable/replicate set the below (event) flag to True)
	# Note that this section had 2 methods (ie., Top and Weighted). If Top is needed, enable event_top flag, else enable event_weighted flag. Set both to True for both (top and weighted)
	event, event_top, event_weighted = False, False, False

	if event:
		
		# The following example uses "definite submarine" IPv4 links from Nautilus 
		event = { 
				'category_condition': 'oc', 
				'ip_version': 4
				}

		# In the following 3 lines, we load IP to cable mapping, IP to ASN mapping and IP geolocation data from Nautilus
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		if event_top:
			
			# The version where we take just the top cable segment

			cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

			all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

			countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count = generate_cable_segment_to_country_as_maps (cable_segment_to_all_info_map, all_ases)

			cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count)

			metadata = {
						'Cable Segments': list(cable_segment_to_all_info_map.keys()),
						'Cable': all_cables,
						'IP links': all_ip_links,
						'IP': all_ips,
						'AS links': all_as_links,
						'AS': all_ases
						}

			all_data = {'Cable Segment': {'Country': cable_segment_against_country, 'AS': cable_segment_against_ases, 'Intra Inter': cable_segment_intra_inter_as_count},
						'Cable': {'Country': cable_against_country, 'AS': cable_against_ases, 'Intra Inter': cable_intra_inter_as_count},
						'Landing Point': {'Country': landing_point_against_country, 'AS': landing_point_against_ases, 'Intra Inter': landing_point_intra_inter_as_count},
						'Country': {'Country': country_against_country, 'AS': country_against_ases, 'Intra Inter': country_intra_inter_as_count},
						'Metadata': {'Country': countries, 'AS': ases}}

			with open('results/independent_analysis_all_data_top_cable_segment', 'wb') as fp:
				pickle.dump(all_data, fp)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5938, 3259), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259)]},
							 'tag': 'SeaMeWe-5-cut-failure-segments-top_cable_segment'
							 }
			event.update(event_updates)

			# The main function that generates the results for this specific event
			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4177), ('Asia Africa Europe-1 (AAE-1)', 3210, 4361), ('Asia Africa Europe-1 (AAE-1)', 3160, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3210), ('Asia Africa Europe-1 (AAE-1)', 6007, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3160), ('Asia Africa Europe-1 (AAE-1)', 5956, 3160), ('Asia Africa Europe-1 (AAE-1)', 3210, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9118), ('Asia Africa Europe-1 (AAE-1)', 3210, 5956), ('Asia Africa Europe-1 (AAE-1)', 3160, 4361), ('Asia Africa Europe-1 (AAE-1)', 9486, 6007), ('Asia Africa Europe-1 (AAE-1)', 3160, 9118), ('Asia Africa Europe-1 (AAE-1)', 9486, 3160), ('Asia Africa Europe-1 (AAE-1)', 3160, 14639), ('Asia Africa Europe-1 (AAE-1)', 3210, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 6013), ('Asia Africa Europe-1 (AAE-1)', 3210, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 3259), ('Asia Africa Europe-1 (AAE-1)', 14639, 3210), ('Asia Africa Europe-1 (AAE-1)', 3210, 9485), ('Asia Africa Europe-1 (AAE-1)', 4177, 3160), ('Asia Africa Europe-1 (AAE-1)', 6013, 3160), ('Asia Africa Europe-1 (AAE-1)', 9486, 3259), ('Asia Africa Europe-1 (AAE-1)', 3212, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4265), ('Asia Africa Europe-1 (AAE-1)', 9486, 5962), ('Asia Africa Europe-1 (AAE-1)', 3160, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 9485)]},
							 'tag': 'Egypt-cut-failure-segments-top_cable_segment'
							 }
			event.update(event_updates)

			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'landing_points': ['Al Hudaydah, Yemen']},
							 'tag': 'Al-Hudaydah-failure-top_cable_segment'
							 }
			event.update(event_updates)

			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)

		if event_weighted:
			# The version where we take all cables top segments 

			cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map_v2 (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

			all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

			countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count = generate_cable_segment_to_country_as_maps_with_data (cable_segment_to_all_info_map, all_ases)

			cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count)

			metadata = {
						'Cable Segments': list(cable_segment_to_all_info_map.keys()),
						'Cable': all_cables,
						'IP links': all_ip_links,
						'IP': all_ips,
						'AS links': all_as_links,
						'AS': all_ases
						}

			all_data = {'Cable Segment': {'Country': cable_segment_against_country, 'AS': cable_segment_against_ases, 'Intra Inter': cable_segment_intra_inter_as_count},
						'Cable': {'Country': cable_against_country, 'AS': cable_against_ases, 'Intra Inter': cable_intra_inter_as_count},
						'Landing Point': {'Country': landing_point_against_country, 'AS': landing_point_against_ases, 'Intra Inter': landing_point_intra_inter_as_count},
						'Country': {'Country': country_against_country, 'AS': country_against_ases, 'Intra Inter': country_intra_inter_as_count},
						'Metadata': {'Country': countries, 'AS': ases}}

			with open('results/independent_analysis_all_data_all_cable_segment_scored', 'wb') as fp:
				pickle.dump(all_data, fp)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5938, 3259), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259)]},
							 'tag': 'SeaMeWe-5-cut-failure-segments-all_cable_segment_scored'
							 }
			event.update(event_updates)

			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'cable_segments': [('SeaMeWe-5', 5346, 5938), ('SeaMeWe-5', 5346, 15020), ('SeaMeWe-5', 5346, 9486), ('SeaMeWe-5', 5346, 17968), ('SeaMeWe-5', 5346, 5962), ('SeaMeWe-5', 5938, 3214), ('SeaMeWe-5', 5938, 5959), ('SeaMeWe-5', 5346, 6300), ('SeaMeWe-5', 5959, 5962), ('SeaMeWe-5', 5861, 5346), ('SeaMeWe-5', 5346, 15021), ('SeaMeWe-5', 5346, 9269), ('SeaMeWe-5', 5346, 14639), ('SeaMeWe-5', 5959, 15020), ('SeaMeWe-5', 5346, 10684), ('SeaMeWe-5', 5346, 7311), ('SeaMeWe-5', 5346, 3259), ('SeaMeWe-5', 5959, 10684), ('SeaMeWe-5', 14639, 5959), ('SeaMeWe-5', 5938, 9486), ('SeaMeWe-5', 9486, 3259), ('SeaMeWe-5', 5346, 9485), ('SeaMeWe-5', 5959, 5861), ('SeaMeWe-5', 15020, 9486), ('SeaMeWe-5', 5959, 15021), ('SeaMeWe-5', 15020, 3214), ('SeaMeWe-5', 5346, 7681), ('SeaMeWe-5', 6300, 5959), ('SeaMeWe-5', 5959, 17968), ('SeaMeWe-5', 3214, 9486), ('SeaMeWe-5', 14639, 3214), ('SeaMeWe-5', 17968, 3214), ('SeaMeWe-5', 3214, 9269), ('SeaMeWe-5', 3214, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4177), ('Asia Africa Europe-1 (AAE-1)', 3210, 4361), ('Asia Africa Europe-1 (AAE-1)', 3160, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3210), ('Asia Africa Europe-1 (AAE-1)', 6007, 5962), ('Asia Africa Europe-1 (AAE-1)', 6007, 3160), ('Asia Africa Europe-1 (AAE-1)', 5956, 3160), ('Asia Africa Europe-1 (AAE-1)', 3210, 3212), ('Asia Africa Europe-1 (AAE-1)', 3210, 9118), ('Asia Africa Europe-1 (AAE-1)', 3210, 5956), ('Asia Africa Europe-1 (AAE-1)', 3160, 4361), ('Asia Africa Europe-1 (AAE-1)', 9486, 6007), ('Asia Africa Europe-1 (AAE-1)', 3160, 9118), ('Asia Africa Europe-1 (AAE-1)', 9486, 3160), ('Asia Africa Europe-1 (AAE-1)', 3160, 14639), ('Asia Africa Europe-1 (AAE-1)', 3210, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 6013), ('Asia Africa Europe-1 (AAE-1)', 3210, 3259), ('Asia Africa Europe-1 (AAE-1)', 3160, 15019), ('Asia Africa Europe-1 (AAE-1)', 3210, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 3259), ('Asia Africa Europe-1 (AAE-1)', 14639, 3210), ('Asia Africa Europe-1 (AAE-1)', 3210, 9485), ('Asia Africa Europe-1 (AAE-1)', 4177, 3160), ('Asia Africa Europe-1 (AAE-1)', 6013, 3160), ('Asia Africa Europe-1 (AAE-1)', 9486, 3259), ('Asia Africa Europe-1 (AAE-1)', 3212, 9486), ('Asia Africa Europe-1 (AAE-1)', 3210, 4265), ('Asia Africa Europe-1 (AAE-1)', 9486, 5962), ('Asia Africa Europe-1 (AAE-1)', 3160, 5969), ('Asia Africa Europe-1 (AAE-1)', 3160, 9485)]},
							 'tag': 'Egypt-cut-failure-segments-all_cable_segment_scored'
							 }
			event.update(event_updates)

			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)

			event_updates = {
							 'type': 'event',
							 'name': None,
							 'baseline': None,
							 'search_radius': None,
							 'additional_threshold': None,
							 'scope': {'landing_points': ['Al Hudaydah, Yemen']},
							 'tag': 'Al-Hudaydah-failure-all_cable_segment_scored'
							 }
			event.update(event_updates)

			individual_event_process_func (event, metadata, cable_segment_to_all_info_map, countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, print_top=10, print_res=False)


	# Section 6 results data (sensitivity analysis) (to enable/replicate set the below flag to True)
	sensitivity_analysis = False 

	if sensitivity_analysis:

		# Definite submarine IPv4 links
		event = { 
				'category_condition': 'oc', 
				'ip_version': 4
				}

		# Downloading relevant data from Nautilus (cable mapping, IP-ASN, Geolocation)
		conditioned_nautilus_mapping = load_nautilus_mapping (ip_version=event['ip_version'], condition=event['category_condition'])
		ip_to_asn_map = generate_ip_to_asn_map (ip_version=event['ip_version'])
		ip_to_best_geolocation = get_best_geolocation_for_all_ips (ip_version=event['ip_version'])

		# Concise representation generation
		cable_segment_to_all_info_map = generate_cable_segments_to_all_info_map (conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation)

		all_cables, all_ip_links, all_ips, all_as_links, all_ases = get_full_stats_for_each_layer (cable_segment_to_all_info_map)

		metadata = {
					'Cable Segments': list(cable_segment_to_all_info_map.keys()),
					'Cable': all_cables,
					'IP links': all_ip_links,
					'IP': all_ips,
					'AS links': all_as_links,
					'AS': all_ases
					}

		# Sensitivity parameters considered
		accuracies_list = [[1, 0, 0], [0.9, 0.05, 0.05], [0.85, 0.1, 0.05], [0.77, 0.19, 0.04], [0.6, 0.3, 0.1], [0.8, 0, 0.2], [0.7, 0, 0.3], [0.6, 0, 0.4]]

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['jp']},
						 'tag': 'Japan-earthquake'
						 }
		event.update(event_updates)

		# This function is similar to earlier function, but not considers all possible sensitivity parameters to generate the results
		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['tw', 'ph']},
						 'tag': 'TW-PH-earthquake'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'regions': ['Washington', 'Oregon', 'British Columbia']},
						 'tag': 'PNW-earthquake'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['id']},
						 'tag': 'Indonesia-earthquake'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		# Next all the hurricanes

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['cu', 'jm', 'ht', 'do', 'bs']},
						 'tag': 'Carribbean-Hurricane'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['gb', 'ie']},
						 'tag': 'British-Isles-Hurricane'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'country_codes': ['bz', 'hn', 'ni', 'pa', 'cr']},
						 'tag': 'Central-America-Hurricane'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'type': 'disaster',
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'scope': {'regions': ['Florida', 'Louisiana', 'Texas']},
						 'tag': 'Florida-Hurricane'
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'name': 'earthquake',
						 'baseline': 6,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'tag': 'Earthquake-All',
						 'scope': None
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'name': 'hurricane',
						 'baseline': 60,
						 'search_radius': 50,
						 'additional_threshold': {'special': 0},
						 'tag': 'Hurricane-All',
						 'scope': None
						 }
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'name': 'searise',
						 'baseline': 0.3,
						 'search_radius': 10,
						 'additional_threshold': {'special': 0},
						 'tag': 'Searise-All',
						 'scope': None
						}
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

		event_updates = {
						 'name': 'solarstorm',
						 'baseline': 50,
						 'search_radius': 1,
						 'additional_threshold': {'special': 0},
						 'tag': 'Solarstorm-All',
						 'scope': None
						}
		event.update(event_updates)

		global_process_inaccuracies (accuracies_list, cable_segment_to_all_info_map, conditioned_nautilus_mapping, ip_to_asn_map, ip_to_best_geolocation, event, metadata)

