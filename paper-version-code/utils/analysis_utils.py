from utils.nautilus_data_utils import *
from itertools import chain, islice
import random, reverse_geocode, pycountry
import numpy as np
import pandas as pd
import random

from collections import namedtuple, Counter
landing_point_info = namedtuple('landing_point_info',  ['value', 'dist'])

def get_landing_points_given_region (latlon_to_value, radius=10, region=None, additional_thresholds=None, log=None):

	'''
	Inputs
		latlon_to_value -> The default structure of a disaster model which maps the lat-lon tuple to the disaster specific value
		radius -> The zone around a given lat-lon tuple to consider to check if any landing points falls under it, default value is 10 km
		region -> region to identify landing points over
			-> If None, it indicates we consider the entire world 
			-> key -> landing_points -> a list of all landing points (not the IDs) -> should be the string name as in TeleGeography -> If used, no other parameters should be set as we just return after this
			-> key -> countries -> a list of all countries to consider
			-> key -> country_codes -> a list of all country codes to consider 
			-> key -> regions -> a list of sub-divisions within a country to consider
		additional_thresholds -> additional filter based on value, defaults to None
			-> keys -> min, max, special (this can be used to eliminate stuff like 0, [], ..)
			-> more keys -> retain (fraction of latlons to retain), choice (can be random, top, prob_top)

	Outputs
		filter_lp_id_list -> A list of all identified landing points satisfying the constraints
	'''

	landing_points = load_landing_points_dict()
	
	filter_lp_id_list = []

	# This is straight forward, map the name to lp id and return 
	if region and 'landing_points' in region:
		landing_point_location_to_lp_id = {v.location: k for k, v in landing_points.items()}
		for item in region['landing_points']:
			lp_id = landing_point_location_to_lp_id.get(item, None)
			if lp_id:
				filter_lp_id_list.append(lp_id)

		print (f'Length of filter lp id list: {len(filter_lp_id_list)}')

		return filter_lp_id_list

	latlon_to_lp_id, tree = construct_balltree(landing_points)
	latlon_rev_geo_result = identify_reverse_geolocation_information_for_all_landing_points(latlon_to_lp_id, log)

	# First let's apply the additional thresholds if any
	if additional_thresholds:
		latlon_to_value = {latlon: value for latlon, value in latlon_to_value.items() 
							if (('special' in additional_thresholds and value != additional_thresholds['special']) or ('special' not in additional_thresholds)) and
							(('min' in additional_thresholds and value >= additional_thresholds['min']) or ('min' not in additional_thresholds)) and 
							(('max' in additional_thresholds and value <= additional_thresholds['max']) or ('max' not in additional_thresholds))} 

	if log:
		print (f'After applying additional thresholds, the length of latlon to value is {len(latlon_to_value)}')

	# Now comes the region selection part
	
	# We will use this to eliminate re-examining for the same landing point
	examined_latlons = []
	filter_lp_id_list_temp = []
	sub_dis_latlon_to_lp_ids_list = {}

	for count, (dis_latlons, value) in enumerate(latlon_to_value.items()):
		dist, landing_point_lat_lon, landing_point_id = find_closest_lp_for_given_latlons (latlon_to_lp_id, tree, dis_latlons, radius=radius)
		matched_lp_ids = []

		for index, sub_latlon in enumerate(landing_point_lat_lon):
			if sub_latlon not in examined_latlons:
				rev_geo = latlon_rev_geo_result.get(sub_latlon, None)

				# If not region (ie., global), no additional checks needed
				# If region is given , check either country code or country or regions match
				if rev_geo and 'country_code' in rev_geo['address'] and 'country' in rev_geo['address']:
					if (not region) or \
						(region and ((('country_codes' in region) and (any(item.lower() == rev_geo['address']['country_code'].lower() for item in region['country_codes']))) or 
						   (('countries' in region) and (any(item.lower() == rev_geo['address']['country'].lower() for item in region['countries']))) or 
						   (('regions' in region) and (any(item.lower() in [i.lower() for i in rev_geo['address'].values()] for item in region['regions']))))):

						filter_lp_id_list_temp.append(landing_point_id[index])
						matched_lp_ids.append(landing_point_id[index])

				examined_latlons.append(sub_latlon)	
			
			else:
				if landing_point_id[index] in filter_lp_id_list_temp:
					matched_lp_ids.append(landing_point_id[index])

		if len(matched_lp_ids) > 0:
			sub_dis_latlon_to_lp_ids_list[dis_latlons] = matched_lp_ids

	if 'retain' in additional_thresholds and 'choice' in additional_thresholds:
		num_to_pick = int(len(sub_dis_latlon_to_lp_ids_list) * additional_thresholds['retain'])
		sub_dis_latlon_to_values = {k: latlon_to_value[k] for k in sub_dis_latlon_to_lp_ids_list}
		sub_dis_latlon_to_values_sorted = dict(sorted(sub_dis_latlon_to_values.items(), key=lambda item: item[1], reverse=True))

		if additional_thresholds['choice'] == 'random':
			# In this we just want to select n random elements from the disaster latlons
			selected_dis_latlons = random.sample(list(sub_dis_latlon_to_lp_ids_list.keys()), num_to_pick)
		elif additional_thresholds['choice'] == 'top':
			# Here we pick the disaster latlons with top n% of values
			selected_dis_latlons = list(sub_dis_latlon_to_values_sorted.keys())[:num_to_pick]
		elif additional_thresholds['choice'] == 'prob_top':
			# Here we mix random with the value, which implies higher value means more chance of getting pickled
			total_value_sum = sum(sub_dis_latlon_to_values_sorted.values())
			probabilities = [value / total_value_sum for value in sub_dis_latlon_to_values_sorted.values()]
			# Choices seems to have in-built replacement, so let's get one by one and check
			# selected_dis_latlons = random.choices(list(sub_dis_latlon_to_values_sorted.keys()), weights=probabilities, k=num_to_pick)
			# So instead we use numpy random choice
			keys = list(sub_dis_latlon_to_values_sorted.keys())
			selected_dis_latlons_index_array = np.random.choice(len(keys), size=num_to_pick, replace=False, p=probabilities)
			selected_dis_latlons = [keys[i] for i in selected_dis_latlons_index_array]
		elif additional_thresholds['choice'] == 'prob_top_rev':
			# Here we mix random with the value, which implies lower value means more chance of getting pickled
			total_value_sum = sum(sub_dis_latlon_to_values_sorted.values())
			probabilities = [1.0 - (value / total_value_sum) for value in sub_dis_latlon_to_values_sorted.values()]
			sum_probabilities = sum(probabilities)
			probabilities = [p / sum_probabilities for p in probabilities]
			keys = list(sub_dis_latlon_to_values_sorted.keys())
			selected_dis_latlons_index_array = np.random.choice(len(keys), size=num_to_pick, replace=False, p=probabilities)
			selected_dis_latlons = [keys[i] for i in selected_dis_latlons_index_array]
		else:
			print ('No valid choice presented, we will select at random by default')
			selected_dis_latlons = random.choices(list(sub_dis_latlon_to_lp_ids_list.keys()), k=num_to_pick)

		filter_lp_id_set = set()
		for dis_latlons in list(selected_dis_latlons):
			filter_lp_id_set.update(sub_dis_latlon_to_lp_ids_list[dis_latlons])
		filter_lp_id_list = list(filter_lp_id_set)

	else:
		filter_lp_id_list = filter_lp_id_list_temp

	return filter_lp_id_list


def merge_ab_ba_as_links (as_links_set):
	merged_set = set()

	for as_link in list(as_links_set):
		if as_link not in merged_set and tuple(reversed(as_link)) not in merged_set:
			merged_set.add(as_link)

	return merged_set


def get_full_stats_for_each_layer (cable_segment_to_all_info_map):

	# The # of cables are independent of the Nautilus mapping
	landing_points = load_landing_points_dict()
	all_cables = set(chain(*[item.cable for item in landing_points.values()]))

	# These give the uniq IP links and IPs as extracted from the conditioned nautilus mapping
	all_ip_links = set(chain(*[item[0] for item in cable_segment_to_all_info_map.values()]))
	all_ips = set(chain(*[list(item) for item in list(all_ip_links)]))

	# These give the uniq AS links and ASes as extracted from the conditioned nautilus mapping
	all_as_links = set(chain(*[item[1] for item in cable_segment_to_all_info_map.values()]))
	# Merging any A-B B-A sort of AS links
	all_as_links = merge_ab_ba_as_links(all_as_links)
	all_ases = set(chain(*[list(item) for item in list(all_as_links)]))

	return (all_cables, all_ip_links, all_ips, all_as_links, all_ases)


def get_coverage_stats_given_scope (cable_segment_to_all_info_map, latlon_to_value=None, radius=10, region=None, additional_thresholds=None, count=0, log=None):

	all_segments, all_cables, all_ip_links, all_ips, all_as_links, all_ases = set(), set(), set(), set(), set(), set()

	if region and 'cable_segment_partial' in region:

		landing_points = load_landing_points_dict()
		landing_point_location_to_lp_id = {v.location: k for k, v in landing_points.items()}
		lp_id = landing_point_location_to_lp_id.get(region['cable_segment_partial'][1], None)

		cable = region['cable_segment_partial'][0]
		all_cables.add(cable)

		if lp_id:
			csp = (cable, lp_id)
			matching_segment = [item for item in cable_segment_to_all_info_map if len(set(csp) & set(item)) == 2]

			if len(matching_segment) > 0:
				all_segments.add(matching_segment[0])

				all_info = cable_segment_to_all_info_map[matching_segment[0]]
				
				filt_ip_links = set(all_info[0])
				all_ip_links.update(filt_ip_links)
				filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
				all_ips.update(filt_ips)
				
				filt_as_links = set(all_info[1])
				all_as_links.update(filt_as_links)
				filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
				all_ases.update(filt_ases)
			else:
				print (f'No match found for given cable segment: {region["cable_segment_partial"]}')
		else:
			print (f'Invalid landing point provided')

	if region and 'cable_segment' in region:
		cable = region['cable_segment'][0]
		all_cables.add(cable)
		matching_segment = [item for item in cable_segment_to_all_info_map if set(region['cable_segment']) == set(item)]

		if len(matching_segment) == 1:
			all_segments.add(matching_segment[0])

			all_info = cable_segment_to_all_info_map[matching_segment[0]]
			
			filt_ip_links = set(all_info[0])
			all_ip_links.update(filt_ip_links)
			filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
			all_ips.update(filt_ips)
			
			filt_as_links = set(all_info[1])
			all_as_links.update(filt_as_links)
			filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
			all_ases.update(filt_ases)

		elif len(matching_segment) == 2:
			print ('Something went wrong and we got 2 cable segment matches, check if cable_segment_map is correctly generated')

		else:
			print (f'No match found for given cable segment: {region["cable_segment"]}')

	if region and 'cable_segments' in region:
		cables = set([item[0] for item in region['cable_segments']])
		all_cables.update(cables)
		matching_segments = [item for item in cable_segment_to_all_info_map if any([set(item) == set(cable_segment) for cable_segment in region['cable_segments']])]

		if len(matching_segments) > 0:
			for segment in matching_segments:
				all_segments.add(segment)

				all_info = cable_segment_to_all_info_map[segment]

				filt_ip_links = set(all_info[0])
				all_ip_links.update(filt_ip_links)
				filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
				all_ips.update(filt_ips)
				
				filt_as_links = set(all_info[1])
				all_as_links.update(filt_as_links)
				filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
				all_ases.update(filt_ases)

		else:

			print (f'No match found for cable: {region["cable"]}')

	if region and 'cable' in region:
		cable = region['cable']
		#all_cables.add(cable)
		all_cables.update(cable)
		# matching_segments = [item for item in cable_segment_to_all_info_map if item[0] == cable]
		matching_segments = [item for item in cable_segment_to_all_info_map if item[0] in cable]

		if len(matching_segments) > 0:

			for segment in matching_segments:
				all_segments.add(segment)

				all_info = cable_segment_to_all_info_map[segment]

				filt_ip_links = set(all_info[0])
				all_ip_links.update(filt_ip_links)
				filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
				all_ips.update(filt_ips)
				
				filt_as_links = set(all_info[1])
				all_as_links.update(filt_as_links)
				filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
				all_ases.update(filt_ases)

		else:

			print (f'No match found for cable: {region["cable"]}')

	if (not region) or (len(set(['landing_points', 'country_codes', 'countries', 'regions']) & set(region.keys())) > 0):

		if not latlon_to_value:
			print ('Provide the latlon to value for given event/disaster. Now we are using the default latlon distribution with 0.1 degree tiles')
			# Essentially putting a default value of 1 for all latlon tiles with 0.1 distance
			latlon_to_value = {}
			for latitude in range(-900, 900, 1):
				for longitude in range(-1800, 1800, 1):
					latlon_to_value[(latitude/10, longitude/10)] = 1

		filter_lp_id_list = get_landing_points_given_region (latlon_to_value, radius, region, additional_thresholds, log)
		
		if log:
			print (f'We have {len(filter_lp_id_list)} matches')

		matching_segments = [item for item in cable_segment_to_all_info_map if len(set(item) & set(filter_lp_id_list)) > 0]

		for segment in matching_segments:
			all_segments.add(segment)

			all_info = cable_segment_to_all_info_map[segment]

			cable = segment[0]
			all_cables.add(cable)

			filt_ip_links = set(all_info[0])
			all_ip_links.update(filt_ip_links)
			filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
			all_ips.update(filt_ips)
			
			filt_as_links = set(all_info[1])
			all_as_links.update(filt_as_links)
			filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
			all_ases.update(filt_ases)

	# Updating the as_links to merge A-B and B-A sort of links
	all_as_links = merge_ab_ba_as_links(all_as_links)

	print (f'Finished processing for {count} combination')

	return (all_segments, all_cables, all_ip_links, all_ips, all_as_links, all_ases)


def get_coverage_stats_given_scope_inaccuracy_accounted (cable_segment_to_all_info_map, cable_segment_to_all_info_map_random, accuracies=[1, 0, 0], latlon_to_value=None, radius=10, region=None, additional_thresholds=None, count=0, log=None):

	all_segments, all_cables, all_ip_links, all_ips, all_as_links, all_ases = set(), set(), set(), set(), set(), set()

	if not latlon_to_value:
		print ('Provide the latlon to value for given event/disaster. Now we are using the default latlon distribution with 0.1 degree tiles')
		# Essentially putting a default value of 1 for all latlon tiles with 0.1 distance
		latlon_to_value = {}
		for latitude in range(-900, 900, 1):
			for longitude in range(-1800, 1800, 1):
				latlon_to_value[(latitude/10, longitude/10)] = 1

	filter_lp_id_list = get_landing_points_given_region (latlon_to_value, radius, region, additional_thresholds, log)
		
	if log:
		print (f'We have {len(filter_lp_id_list)} matches')

	matching_segments_orig = [item for item in cable_segment_to_all_info_map if len(set(item) & set(filter_lp_id_list)) > 0]
	matching_segments_random = [item for item in cable_segment_to_all_info_map_random if len(set(item) & set(filter_lp_id_list)) > 0]

	matching_segments = list(set(matching_segments_orig + matching_segments_random))

	correct_num = round(len(matching_segments) * accuracies[0])
	incorrect_num = round(len(matching_segments) * accuracies[1])
	skip_num = len(matching_segments) - correct_num - incorrect_num

	correct = random.sample(matching_segments, correct_num)
	leftover = list(set(matching_segments) - set(correct))
	incorrect = random.sample(leftover, incorrect_num)
	skip = list(set(leftover) - set(incorrect))

	for segment in correct:
		all_segments.add(segment)

		all_info = cable_segment_to_all_info_map.get(segment, None)

		if all_info:
			cable = segment[0]
			all_cables.add(cable)

			filt_ip_links = set(all_info[0])
			all_ip_links.update(filt_ip_links)
			filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
			all_ips.update(filt_ips)
			
			filt_as_links = set(all_info[1])
			all_as_links.update(filt_as_links)
			filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
			all_ases.update(filt_ases)

	for segment in incorrect:
		all_segments.add(segment)

		all_info = cable_segment_to_all_info_map_random.get(segment, None)

		if all_info:
			cable = segment[0]
			all_cables.add(cable)

			filt_ip_links = set(all_info[0])
			all_ip_links.update(filt_ip_links)
			filt_ips = set(chain(*[list(item) for item in list(filt_ip_links)]))
			all_ips.update(filt_ips)
			
			filt_as_links = set(all_info[1])
			all_as_links.update(filt_as_links)
			filt_ases = set(chain(*[list(item) for item in list(filt_as_links)]))
			all_ases.update(filt_ases)

	# Updating the as_links to merge A-B and B-A sort of links
	all_as_links = merge_ab_ba_as_links(all_as_links)

	print (f'Finished processing for {count} combination with {len(all_segments)} segments')

	return (all_segments, all_cables, all_ip_links, all_ips, all_as_links, all_ases)	


def generate_network_country_to_intra_inter_as_links (cable_segment_to_all_info_map, impacted_segments=None):

	country_to_intra_inter_as_count = {}
	for segment, values in cable_segment_to_all_info_map.items():
		if (impacted_segments and segment in impacted_segments) or (not impacted_segments):
			for as_link, geolocation in zip(values[1], values[2]):
				intra = (as_link[0] == as_link[1])
				if geolocation[0] != '+':
					left_country = reverse_geocode.search([geolocation[0]])[0]['country_code']
					current = country_to_intra_inter_as_count.get(left_country, [0, 0])
					current = [current[0]+1, current[1]] if intra else [current[0], current[1]+1]
					country_to_intra_inter_as_count[left_country] = current
				if geolocation[1] != '+':
					right_country = reverse_geocode.search([geolocation[1]])[0]['country_code']
					current = country_to_intra_inter_as_count.get(right_country, [0, 0])
					current = [current[0]+1, current[1]] if intra else [current[0], current[1]+1]
					country_to_intra_inter_as_count[right_country] = current

	return country_to_intra_inter_as_count


def generate_cable_segment_to_country_as_maps (cable_segment_to_all_info_map, all_ases):

	countries = [country.alpha_2 for country in pycountry.countries] + ['XK']
	countries_set = set(countries)
	ases = list(all_ases)

	# The values against each country/AS will be essentially be the number of IP links with an endpoint in that country
	cable_segment_against_country = {}
	cable_segment_against_ases = {}
	cable_segment_intra_inter_as_count = {}

	for segment, values in cable_segment_to_all_info_map.items():
		geolocation = values[2]
		all_ips_geolocations = list(chain(*[[i for i in item if i != '+'] for item in geolocation]))
		rev_geo_res = reverse_geocode.search(all_ips_geolocations)
		local_countries = [item['country_code'] for item in rev_geo_res]
		local_countries_counter = dict(Counter(local_countries))

		cable_segment_against_country[segment] = [local_countries_counter.get(country, 0) for country in countries]

		as_links = values[1]
		all_ases = list(chain(*[list(item) for item in as_links]))
		all_ases_counter = dict(Counter(all_ases))

		cable_segment_against_ases[segment] = [all_ases_counter.get(AS, 0) for AS in ases]

		intra, inter = [], []

		for as_link in as_links:
			if as_link[0] == as_link[1]:
				intra.append(as_link)
			else:
				inter.append(as_link)

		cable_segment_intra_inter_as_count[segment] = (intra, inter)

	return countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count


def combine_entry_with_counts (entry, counts):
	ret_dict = {}
	for key, value in zip(entry, counts):
		if key in ret_dict:
			ret_dict[key] += value
		else:
			ret_dict[key] = value

	return ret_dict


def generate_cable_segment_to_country_as_maps_with_data (cable_segment_to_all_info_map, all_ases):

	countries = [country.alpha_2 for country in pycountry.countries] + ['XK']
	countries_set = set(countries)
	ases = list(all_ases)

	# The values against each country/AS will be essentially be the number of IP links with an endpoint in that country
	cable_segment_against_country = {}
	cable_segment_against_ases = {}
	cable_segment_intra_inter_as_count = {}

	for segment, values in cable_segment_to_all_info_map.items():
		geolocation = values[2]
		all_ips_geolocations = list(chain(*[[i for i in item if i != '+'] for item in geolocation]))
		all_ips_geolocations = []
		geo_scores = []
		for index, item in enumerate(geolocation):
			for i in item:
				if i != '+':
					all_ips_geolocations.append(i)
					geo_scores.append(values[4][index])
		rev_geo_res = reverse_geocode.search(all_ips_geolocations)
		local_countries = [item['country_code'] for item in rev_geo_res]
		local_countries_counter = combine_entry_with_counts(local_countries, geo_scores)

		cable_segment_against_country[segment] = [local_countries_counter.get(country, 0) for country in countries]

		as_links = values[1]
		all_as = []
		as_scores = []
		for index, item in enumerate(as_links):
			for i in item:
				all_as.append(i)
				as_scores.append(values[4][index])
		all_ases_counter = combine_entry_with_counts(all_as, as_scores)

		cable_segment_against_ases[segment] = [all_ases_counter.get(AS, 0) for AS in ases]

		intra, inter = [], []

		for as_link in as_links:
			if as_link[0] == as_link[1]:
				intra.append(as_link)
			else:
				inter.append(as_link)

		cable_segment_intra_inter_as_count[segment] = (intra, inter)

	return countries, ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count


def generate_all_other_levels_to_country_as_maps (cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count):

	uniq_cables = list(set([item[0] for item in cable_segment_against_country.keys()]))
	uniq_landing_points = list(set(chain(*[item[1:] for item in cable_segment_against_country.keys()])))

	# For uniq countries, we do it based on the landing points
	landing_points = load_landing_points_dict()
	_, country_to_lp_id_dict = get_lp_id_to_country_dict (landing_points)
	uniq_countries = list(country_to_lp_id_dict.keys())

	cable_against_country = {}
	cable_against_ases = {}
	cable_intra_inter_as_count = {}

	for cable in uniq_cables:
		matching_segments = [item for item in cable_segment_against_country.keys() if item[0] == cable]
		countries_list = [cable_segment_against_country[item] for item in matching_segments]
		ases_list = [cable_segment_against_ases[item] for item in matching_segments]
		intra_inter_list = [list(cable_segment_intra_inter_as_count[item]) for item in matching_segments]
		cable_against_country[cable] = [sum(x) for x in zip(*countries_list)]
		cable_against_ases[cable] = [sum(x) for x in zip(*ases_list)]
		cable_intra_inter_as_count[cable] = tuple([sum(x, []) for x in zip(*intra_inter_list)])

	landing_point_against_country = {}
	landing_point_against_ases = {}
	landing_point_intra_inter_as_count = {}

	for landing_point in uniq_landing_points:
		matching_segments = [item for item in cable_segment_against_country.keys() if landing_point in item]
		countries_list = [cable_segment_against_country[item] for item in matching_segments]
		ases_list = [cable_segment_against_ases[item] for item in matching_segments]
		intra_inter_list = [list(cable_segment_intra_inter_as_count[item]) for item in matching_segments]
		landing_point_against_country[landing_point] = [sum(x) for x in zip(*countries_list)]
		landing_point_against_ases[landing_point] = [sum(x) for x in zip(*ases_list)]
		landing_point_intra_inter_as_count[landing_point] = tuple([sum(x, []) for x in zip(*intra_inter_list)])

	country_against_country = {}
	country_against_ases = {}
	country_intra_inter_as_count = {}

	for country in uniq_countries:
		matching_segments = [item for item in cable_segment_against_country.keys() if len(set(item) & set(country_to_lp_id_dict[country])) > 0]
		if len(matching_segments) > 0:
			countries_list = [cable_segment_against_country[item] for item in matching_segments]
			ases_list = [cable_segment_against_ases[item] for item in matching_segments]
			intra_inter_list = [list(cable_segment_intra_inter_as_count[item]) for item in matching_segments]
			country_against_country[country] = [sum(x) for x in zip(*countries_list)]
			country_against_ases[country] = [sum(x) for x in zip(*ases_list)]
			country_intra_inter_as_count[country] = tuple([sum(x, []) for x in zip(*intra_inter_list)])

	return cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count


def normalize_based_on_column (analysis_dict_to_list_map):
	sum_across_columns = [sum(x) for x in zip(*list(analysis_dict_to_list_map.values()))]
	return_normalized_dict_to_list = {}

	for key, value in analysis_dict_to_list_map.items():
		return_normalized_dict_to_list[key] = [item/sum_across_columns[index] if (len(value) > 0 and sum_across_columns[index] != 0) else 0 for index, item in enumerate(value)]

	return return_normalized_dict_to_list


def sorted_result_print_and_return (query_dict, print_top=10):

	# This is straight forward summation, when we have a non-normalized dict
	# If it normalized, we essentially are assigning equal value to all columns (ie., country or AS)
	query_dict_sum = {k:sum(v) for k,v in query_dict.items()}
	query_dict_sum_sorted = {k:v for k,v in sorted(query_dict_sum.items(), key=lambda x:x[1], reverse=True)}
	print (f'\t{list(islice(query_dict_sum_sorted.items(), print_top))}')
	return query_dict_sum_sorted


def ranker_function (countries, all_ases, cable_segment_against_country, cable_segment_against_ases, cable_segment_intra_inter_as_count, impacted_segments, print_top=10, print_res=False):

	return_dict = {}

	# These will be the basic version (no normalizing case)
	cable_segment_against_country_mod = {k:v for k,v in cable_segment_against_country.items() if k in impacted_segments}
	cable_segment_against_ases_mod = {k:v for k,v in cable_segment_against_ases.items() if k in impacted_segments}
	cable_segment_intra_inter_as_count_mod = {k:v for k,v in cable_segment_intra_inter_as_count.items() if k in impacted_segments}

	cable_against_country, cable_against_ases, cable_intra_inter_as_count, landing_point_against_country, landing_point_against_ases, landing_point_intra_inter_as_count, country_against_country, country_against_ases, country_intra_inter_as_count = generate_all_other_levels_to_country_as_maps(cable_segment_against_country_mod, cable_segment_against_ases_mod, cable_segment_intra_inter_as_count_mod)

	return_dict['Regular'] = {
								'Cable Segment': {'Country': cable_segment_against_country_mod, 'AS': cable_segment_against_ases_mod},
								'Cable': {'Country': cable_against_country, 'AS': cable_against_ases},
								'Landing Point': {'Country': landing_point_against_country, 'AS': landing_point_against_ases},
								'Country': {'Country': country_against_country, 'AS': country_against_ases}
							 }

	if print_res:
		print ('*' * 50)
		print (f'Top {print_top} results non-normalized')
		print ('Results against countries now')
		print (f'\tAt cable segment')
		sorted_result_print_and_return(cable_segment_against_country_mod)
		print (f'\tAt cable')
		sorted_result_print_and_return(cable_against_country)
		print (f'\tAt landing point')
		sorted_result_print_and_return(landing_point_against_country)
		print (f'\tAt country')
		sorted_result_print_and_return(country_against_country)
		print ('Results against AS now')
		print (f'\tAt cable segment')
		sorted_result_print_and_return(cable_segment_against_ases_mod)
		print (f'\tAt cable')
		sorted_result_print_and_return(cable_against_ases)
		print (f'\tAt landing point')
		sorted_result_print_and_return(landing_point_against_ases)
		print (f'\tAt country')
		sorted_result_print_and_return(country_against_ases)
		print ('*' * 50)

	# These are the normalized versions (applicable only to against country and ASes)
	cable_segment_against_country_norm = normalize_based_on_column(cable_segment_against_country)
	cable_segment_against_ases_norm = normalize_based_on_column(cable_segment_against_ases)
	# cable_segment_intra_inter_as_count_norm = normalize_based_on_column(cable_segment_intra_inter_as_count) # This might need some more investigation

	cable_segment_against_country_norm_mod = {k:v for k,v in cable_segment_against_country_norm.items() if k in impacted_segments}
	cable_segment_against_ases_norm_mod = {k:v for k,v in cable_segment_against_ases_norm.items() if k in impacted_segments}
	# cable_segment_intra_inter_as_count_norm_mod = {k:v for k,v in cable_segment_intra_inter_as_count_norm.items() if k in impacted_segments}

	cable_against_country_norm, cable_against_ases_norm, cable_intra_inter_as_count_norm, landing_point_against_country_norm, landing_point_against_ases_norm, landing_point_intra_inter_as_count_norm, country_against_country_norm, country_against_ases_norm, country_intra_inter_as_count_norm = generate_all_other_levels_to_country_as_maps(cable_segment_against_country_norm_mod, cable_segment_against_ases_norm_mod, cable_segment_intra_inter_as_count_mod)

	return_dict['Normalized'] = {
								'Cable Segment': {'Country': cable_segment_against_country_norm_mod, 'AS': cable_segment_against_ases_norm_mod},
								'Cable': {'Country': cable_against_country_norm, 'AS': cable_against_ases_norm},
								'Landing Point': {'Country': landing_point_against_country_norm, 'AS': landing_point_against_ases_norm},
								'Country': {'Country': country_against_country_norm, 'AS': country_against_ases_norm}
							 }

	if print_res:
		print ('*' * 50)
		print (f'Top {print_top} results normalized')
		print ('Results against countries now')
		print (f'\tAt cable segment')
		sorted_result_print_and_return(cable_segment_against_country_norm_mod)
		print (f'\tAt cable')
		sorted_result_print_and_return(cable_against_country_norm)
		print (f'\tAt landing point')
		sorted_result_print_and_return(landing_point_against_country_norm)
		print (f'\tAt country')
		sorted_result_print_and_return(country_against_country_norm)
		print ('Results against AS now')
		print (f'\tAt cable segment')
		sorted_result_print_and_return(cable_segment_against_ases_norm_mod)
		print (f'\tAt cable')
		sorted_result_print_and_return(cable_against_ases_norm)
		print (f'\tAt landing point')
		sorted_result_print_and_return(landing_point_against_ases_norm)
		print (f'\tAt country')
		sorted_result_print_and_return(country_against_ases_norm)
		print ('*' * 50)

	summed_against_country = [sum(x) for x in zip(*(list(cable_segment_against_country_mod.values())))]
	if len(summed_against_country) > 0:
		country_to_count = {country: summed_against_country[index] for index, country in enumerate(countries)}
		country_to_count_sorted = {country:value for country, value in sorted(country_to_count.items(), key=lambda x:x[1], reverse=True)}
	else:
		country_to_count_sorted = {country: 0 for country in list(countries)}
	summed_against_country_norm = [sum(x) for x in zip(*(list(cable_segment_against_country_norm_mod.values())))]
	if len(summed_against_country_norm) > 0:
		country_to_count_norm = {country: summed_against_country_norm[index] for index, country in enumerate(countries)}
		country_to_count_norm_sorted = {country:value for country, value in sorted(country_to_count_norm.items(), key=lambda x:x[1], reverse=True)}
	else:
		country_to_count_norm_sorted = {country: 0 for country in list(countries)}

	summed_against_ases = [sum(x) for x in zip(*(list(cable_segment_against_ases_mod.values())))]
	if len(summed_against_ases) > 0:
		ases_to_count = {ases: summed_against_ases[index] for index, ases in enumerate(all_ases)}
		ases_to_count_sorted = {ases:value for ases, value in sorted(ases_to_count.items(), key=lambda x:x[1], reverse=True)}
	else:
		ases_to_count_sorted = {AS: 0 for AS in list(all_ases)}
	summed_against_ases_norm = [sum(x) for x in zip(*(list(cable_segment_against_ases_norm_mod.values())))]
	if len(summed_against_ases_norm) > 0:
		ases_to_count_norm = {ases: summed_against_ases_norm[index] for index, ases in enumerate(all_ases)}
		ases_to_count_norm_sorted = {ases:value for ases, value in sorted(ases_to_count_norm.items(), key=lambda x:x[1], reverse=True)}
	else:
		ases_to_count_norm_sorted = {AS: 0 for AS in list(all_ases)}

	if print_res:
		print ('*' * 50)
		print (f'Now ranking the top {print_top} influential elements for the event')
		print (f'For countries')
		print (f'\tNon-normalized -> {list(islice(country_to_count_sorted.items(), print_top))}')
		print (f'\tNormalized -> {list(islice(country_to_count_norm_sorted.items(), print_top))}')
		print ('*' * 50)
		print (f'For ASes')
		print (f'\tNon-normalized -> {list(islice(ases_to_count_sorted.items(), print_top))}')
		print (f'\tNormalized -> {list(islice(ases_to_count_norm_sorted.items(), print_top))}')
		print ('*' * 50)

	intra_inter_as_total_count = [sum(x, []) for x in zip(*(list(cable_segment_intra_inter_as_count_mod.values())))]
	if len(intra_inter_as_total_count) > 0:
		intra_total = len(intra_inter_as_total_count[0])
		inter_total = len(intra_inter_as_total_count[1])
		intra_total_uniq = len(set(intra_inter_as_total_count[0]))
		inter_uniq_as_links = set(intra_inter_as_total_count[1])
		inter_uniq_as_links = merge_ab_ba_as_links(inter_uniq_as_links)
		inter_total_uniq = len(inter_uniq_as_links)
	else:
		inter_uniq_as_links = 0

	return_dict['Intra-Inter'] = {'AS-Total': intra_inter_as_total_count, 'Inter-Uniq': inter_uniq_as_links, 'Intra Inter Country': country_intra_inter_as_count}
	
	if print_res:
		print (f'Intra inter count: {intra_total}, {inter_total}, {intra_total_uniq}, {inter_total_uniq}')

	return return_dict


def generate_correlation_dataframe (data, columns):
	df = pd.DataFrame.from_dict(data, orient='index', columns=columns)
	df_filt = df.corr()
	df_filt = df_filt.fillna(0)
	corr_matrix = pd.DataFrame(df_filt)
	return corr_matrix


def get_country_data_for_all_ips (ip_to_best_geolocation):

	ip_to_country = {}
	for ip, value in ip_to_best_geolocation.items():
		country = reverse_geocode.search([value])[0]['country_code']
		ip_to_country[ip] = country

	return ip_to_country


def generate_count_by_country (all_ip_links, ip_to_country):

	count_by_country = {}

	for ip_link in all_ip_links:
		country_left = ip_to_country.get(ip_link[0], None)
		country_right = ip_to_country.get(ip_link[1], None)
		if country_left:
			current = count_by_country.get(country_left, 0)
			current += 1
			count_by_country[country_left] = current
		if country_right:
			current = count_by_country.get(country_right, 0)
			current += 1
			count_by_country[country_right] = current

	return count_by_country
