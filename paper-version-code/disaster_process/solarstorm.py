import math
from utils.nautilus_data_utils import load_landing_points_dict

def load_dataset(baseline_threshold=None, log=False):

	# Let's just base the mapping based on submarine landing point locations
	landing_points = load_landing_points_dict()
	latlon_to_value = {(v.latitude, v.longitude): v.latitude for v in landing_points.values()}

	if baseline_threshold:
		latlon_to_value = {latlon: value for latlon, value in latlon_to_value.items() if value >= baseline_threshold}

	return latlon_to_value, None

if __name__ == '__main__':

	latlon_to_value, _ = load_dataset()