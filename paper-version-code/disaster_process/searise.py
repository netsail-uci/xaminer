import pickle, sys
from pathlib import Path
from global_land_mask import globe # This library is used for computing if point in land or ocean, much faster than shapely, does it based on depth (elevation) in 1 km tiles

def check_node_adjacent_to_ocean (lat_lon_to_ele_land_map, node):

	lat, lon = node[0], node[1]
	near_4_points = [(lat+0.1, lon), (lat-0.1, lon), (lat, lon+0.1), (lat, lon-0.1)]
	
	for point in near_4_points:
		res = lat_lon_to_ele_land_map.get((round(point[0], 1), round(point[1], 1)), (0, True))
		if res[1] == False:
			return True

	return False

def load_dataset (baseline_threshold=None, log=False):

	elevation_map_file = 'datasets/disaster_data/searise/lat_lon_ele_land_map'

	if Path(elevation_map_file).exists():
		with open(elevation_map_file, 'rb') as fp:
			lat_lon_to_ele_land_map = pickle.load(fp)

	else:

		if not Path('datasets/disaster_data/searise/ETOPO1_Bed_g_int.xyz').exists():
			print (f'Download the elevation dataset from "https://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/bedrock/grid_registered/xyz/#:~:text=ETOPO1_Bed_g_int.xyz.gz" and save the file under the datasets/disaster_data/searise folder')
			sys.exit()

		with open('datasets/disaster_data/searise/ETOPO1_Bed_g_int.xyz') as fp:
			contents = fp.readlines()

		# Downsampling by considering 1 in 6 (as ETOP dataset has ~0.0167 difference in consecutive lat, lon)
		updated_contents = contents[::6]

		lat_lon_ele_map = {}
		for count, line in enumerate(updated_contents):

			parts = line.strip().split()
			lon, lat, ele = float(parts[0]), float(parts[1]), int(parts[2])

			lat_lon_ele_map[(lat, lon)] = ele 
			if count % 100000 == 0 and log:
				print (f'Processed {count} entries')

		lat_lon_to_ele_land_map_temp = {}

		for count, (latlon, ele) in enumerate(lat_lon_ele_map.items()):
			lat_lon_to_ele_land_map_temp[latlon] = (ele, globe.is_land(latlon[0], latlon[1]))
			if count % 100000 == 0 and log:
				print (f'Processed {count} entries')

		# Due to strange reason we have more lat, lon data than we expected, downsampling again
		lat_lon_to_ele_land_map = {}

		for latlon, value in lat_lon_to_ele_land_map_temp.items():
			if (latlon[0].is_integer() or len(str(latlon[0]).split('.')[-1]) == 1) and (latlon[1].is_integer() or len(str(latlon[1]).split('.')[-1]) == 1):
				lat_lon_to_ele_land_map[latlon] = value

		with open(elevation_map_file, 'wb') as fp:
			pickle.dump(lat_lon_to_ele_land_map, fp)


	lat_lon_to_ele_map = {latlon: value[0] for latlon, value in lat_lon_to_ele_land_map.items() if value[1] == True and check_node_adjacent_to_ocean(lat_lon_to_ele_land_map, latlon)}
	
	if baseline_threshold:
		lat_lon_to_ele_map = {latlon: value for latlon, value in lat_lon_to_ele_map.items() if value <= baseline_threshold}

	return lat_lon_to_ele_map, None

if __name__ == '__main__':
	lat_lon_to_ele_map, _ = load_dataset()