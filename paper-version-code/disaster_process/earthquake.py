import math, sys
from pathlib import Path

def load_dataset(baseline_threshold=None, log=False):

	'''
	Inputs
		baseline_threshold -> The minimum threshold to be applied 
		log -> Just a logging facility
	Outputs
		latlon_to_mmi -> This is in line with the expected input for SurviveNet
		(ie., latitude and longitude tuple to a value to characterize disaster impact, which is pgs here)
		None -> Here it is None, but this second return output is just provided in case something else needs to be sent over
	'''

	if not Path('datasets/disaster_data/earthquake/GSHPUB.DAT').exists():
		print ('Download the earthquake dataset from "http://static.seismo.ethz.ch/gshap/gshpub.zip" and move the GSHPUB.DAT file to datasets/disaster_data/earthquake folder')
		sys.exit(1)

	with open('datasets/disaster_data/earthquake/GSHPUB.DAT') as fp:
		content = fp.readlines()

	latlon_to_pga = {(float(item.split('\t')[1]), float(item.split('\t')[0])): float(item.split('\t')[2].strip()) for item in content if item.split('\t')[2].strip() not in ('NaN', '0')}

	# Coverting PGA (Peak Ground Acceleration) to MMI (Modified Mercalli Index)

	latlon_to_mmi = {}

	for latlon, pga in latlon_to_pga.items():
		# We will use the equations from worden et. al.
		# Our original data has results in m/s^2, but formula based on cm/s^2
		log_10_value = math.log10(pga * 100)
		if log_10_value <= 1.57:
			mmi = 1.78 + 1.55 * log_10_value
		else:
			mmi = -1.6 + 3.7 * log_10_value

		latlon_to_mmi[latlon] = mmi 

	if baseline_threshold:
		latlon_to_mmi = {latlon: mmi for latlon, mmi in latlon_to_mmi.items() if mmi >= baseline_threshold}

	if log:
		print (f'PGA: Max -> {max(latlon_to_pga.values())} and Min -> {min(latlon_to_pga.values())}')
		print (f'MMI: Max -> {max(latlon_to_mmi.values())} and Min -> {min(latlon_to_mmi.values())}')

	return latlon_to_mmi, None

if __name__ == '__main__':
	latlon_to_mmi, _ = load_dataset()